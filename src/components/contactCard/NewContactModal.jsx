/* eslint-disable no-useless-escape */
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { ModalBody, ModalFooter } from "reactstrap";
import { useSelector } from "react-redux";

const NewContactModal = (props) => {
  const { saving, error } = useSelector((state) => {
    return {
      saving: state.contacts.saving,
      saved: state.contacts.saved,
      error: state.contacts.error,
    };
  });

  const { register, errors, handleSubmit } = useForm();

  const submitForm = (data) => {
    let newObj = formState;
    for (let key in newObj) {
      if (!newObj[key]) {
        delete newObj[key];
      }
    }
    props.onSubmit(formState);
  };

  const [formState, changeFormState] = useState({
    name: "",
    tel: "",
    tel2: "",
    email: "",
    email2: "",
    adress: "",
  });

  const editFormState = (e) => {
    e.preventDefault();
    const { name, value } = e.target;

    changeFormState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  return (
    <div>
      <form>
        <ModalBody>
          <div className="form-group">
            <label htmlFor="exampleFormControlInput1">
              İsim<sup style={{ color: "red" }}>*</sup>
            </label>
            <input
              ref={register({ required: true })}
              maxLength={20}
              className="form-control"
              type="text"
              name="name"
              autoFocus
              onChange={editFormState}
              value={formState.name}
              placeholder={"İsim"}
            />
            {errors.name && (
              <div className="alert alert-danger" role="alert">
                Zorunlu alan
              </div>
            )}
          </div>
          <div className="form-group">
            <label htmlFor="exampleFormControlInput1">
              Telefon - 1<sup style={{ color: "red" }}>*</sup>
            </label>
            <input
              maxLength="10"
              ref={register({
                required: true,
                validate: (value) => {
                  const string = String(value);
                  if (string.length < 10 || string.startsWith("0")) {
                    return false;
                  } else {
                    return true;
                  }
                },
              })}
              className="form-control"
              type="number"
              name="tel"
              onChange={editFormState}
              value={formState.tel}
              placeholder={"555 555 55 55"}
            />
            {errors.tel && (
              <div className="alert alert-danger" role="alert">
                1- Zorunlu Alan 2- Max 10 karakter 3- Örn 555 555 55 55
              </div>
            )}
          </div>
          <div className="form-group">
            <label htmlFor="exampleFormControlInput1">Telefon - 2</label>
            <input
              ref={register({
                validate: (value) => {
                  const string = String(value);
                  if (string) {
                    if (string.length < 10 || string.startsWith("0")) {
                      return false;
                    } else {
                      return true;
                    }
                  }
                },
              })}
              maxLength={10}
              className="form-control"
              type="number"
              name="tel2"
              onChange={editFormState}
              value={formState.tel2}
              placeholder={"Telefon2"}
            />
            {errors.tel2 && (
              <div className="alert alert-danger" role="alert">
                1- Max 10 karakter 2- Örn 555 555 55 55
              </div>
            )}
          </div>
          <div className="form-group">
            <label htmlFor="exampleFormControlInput1">
              Email<sup style={{ color: "red" }}>*</sup>
            </label>
            <input
              ref={register({
                required: true,
                validate: (value) => {
                  const string = String(value);
                  if (
                    string.match(
                      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
                    )
                  ) {
                    return true;
                  } else {
                    return false;
                  }
                },
              })}
              maxLength={100}
              className="form-control"
              type="email"
              name="email"
              onChange={editFormState}
              value={formState.email}
              placeholder={"Email"}
            />
            {errors.email && (
              <div className="alert alert-danger" role="alert">
                1- Zorunlu alan 2- example@example.com
              </div>
            )}
          </div>
          <div className="form-group">
            <label htmlFor="exampleFormControlInput1">Email - 2</label>
            <input
              ref={register({
                validate: (value) => {
                  const string = String(value);
                  if (string) {
                    if (
                      string.match(
                        /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
                      )
                    ) {
                      return true;
                    } else {
                      return false;
                    }
                  }
                },
              })}
              maxLength={100}
              className="form-control"
              type="email"
              name="email2"
              onChange={editFormState}
              value={formState.email2}
              placeholder={"Email2"}
            />
            {errors.email2 && (
              <div className="alert alert-danger" role="alert">
                Düzgün mail ekle ulan
              </div>
            )}
          </div>
          <div className="form-group">
            <label htmlFor="exampleFormControlInput1">Adres</label>
            <textarea
              ref={register()}
              maxLength={200}
              className="form-control"
              type="text"
              rows="3"
              name="adress"
              onChange={editFormState}
              value={formState.adress}
              placeholder={"Adres"}
            />
          </div>
        </ModalBody>
        <ModalFooter>
          {saving ? (
            <div className="spinner-border text-secondary" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          ) : null}

          {error ? (
            <div class="alert alert-danger" role="alert">
              {error}
            </div>
          ) : null}

          <button
            type="button"
            onClick={handleSubmit(submitForm)}
            className="btn btn-dark btn-sm ml-1"
          >
            Ekle
          </button>
          <button
            type="button"
            className="btn btn-secondary btn-sm ml-1"
            onClick={props.closeModal}
          >
            İptal
          </button>
        </ModalFooter>
      </form>
    </div>
  );
};

export default NewContactModal;
