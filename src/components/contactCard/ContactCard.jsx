import React, { useEffect } from "react";
import { Phone, Mail, User, MapPin } from "react-feather";
import EditContactCardModal from "./EditContactCardModal";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteContact, updateContact } from "../../actions/contactActions";
import { Modal, ModalBody, ModalFooter, Button } from "reactstrap";

const ContactCard = (props) => {
  const dispatch = useDispatch();

  const { deleted, updated, updating, error, isSignedIn } = useSelector((state) => {
    return {
      deleted: state.contacts.deleted,
      updated: state.contacts.updated,
      updating: state.contacts.updating,
      error: state.contacts.error,
      isSignedIn: state.auth.isSignedIn,
    };
  });

  const [deleteModal, toggleDeleteModal] = useState(false);
  const [editModal, toggleEditModal] = useState(false);

  const toggleDelete = () => {
    toggleDeleteModal(!deleteModal);
  };
  const toggleEdit = () => {
    toggleEditModal(!editModal);
  };

  const handleUpdate = (updatedForm) => {
    dispatch(updateContact(updatedForm, _id));
  };

  const handleDelete = (e) => {

    dispatch(deleteContact(_id));
  };

  if (props.contact) {
    var { name, tel, tel2, adress, email, email2, _id } = props.contact;
  }

  useEffect(() => {
    if (deleted) {
      toggleDeleteModal(false);
    }
    if (updated) {
      toggleEditModal(false);
    }
  }, [deleted, updated]);

  return (
    <div className="table-responsive">
      <table className="table">
        <tbody>
          <tr>
            <td>
              <User size={16} />
            </td>
            <td>{name && name} </td>
          </tr>
          <tr>
            <td>
              <Phone size={16} />
            </td>
            <td>{tel && tel}</td>
          </tr>
          {tel2 ? (
            <tr>
              <td>
                <Phone size={16} />
              </td>
              <td>{tel2 && tel2}</td>
            </tr>
          ) : null}
          <tr>
            <td>
              <Mail size={16} />
            </td>
            <td>{email && email}</td>
          </tr>
          {email2 ? (
            <tr>
              <td>
                <Mail size={16} />
              </td>
              <td>{email2 && email2}</td>
            </tr>
          ) : null}
          {adress ? (
            <tr>
              <td>
                <MapPin size={16} />
              </td>
              <td>{adress && adress}</td>
            </tr>
          ) : null}
        </tbody>
      </table>
      {((props.showButtons === true) && (isSignedIn === true)) ? (
        <div className="btn-group" role="group" aria-label="Basic example">
          <button
            onClick={toggleDelete}
            type="button"
            className="btn btn-danger btn-sm"
            style={{ width: "70px" }}
          >
            Delete
          </button>
          <button
            onClick={toggleEdit}
            type="button"
            className="btn btn-secondary btn-sm"
            style={{ width: "70px" }}
          >
            Edit
          </button>
        </div>
      ) : null}
      <hr />
      {/* s */}
      <EditContactCardModal
        {...props.contact}
        open={editModal}
        toggle={toggleEdit}
        updating={updating}
        error={error}
        updateConfirmed={handleUpdate}
      />

      {deleteModal ? <Modal isOpen={deleteModal} toggle={toggleDelete}>
        <ModalBody>
          İletişim bilgisini silmek istediğinize emin misiniz?
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={handleDelete}>
            Sil
          </Button>{" "}
          <Button color="secondary" onClick={toggleDelete}>
            İptal
          </Button>
        </ModalFooter>
      </Modal> : null}
    </div>
  );
};

export default ContactCard;
