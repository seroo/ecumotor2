import React from "react";
import { SERVER_IMAGE_URL } from "../../actions/actionConstants";

const PhotoCarousel = (props) => {
  const { photos } = props;
  return (
    <div
      className="carousel slide my-4"
      id="carouselExampleIndicators"
      data-ride="carousel"
    >
      <ol className="carousel-indicators">
        {photos && photos.map((photo, index) => {
          return (
            <li
            key={index + (index * 3)}
              className={`${index === 0 ? "active" : ""}`}
              data-slide-to={index}
              data-target="#carouselExampleIndicators"
            />
          );
        })}
      </ol>

      <div className="carousel-inner" role="listbox">
        {photos && photos.map((photo, index) => {
          return (
            <div
              key={`${index}${photo}`}
              className={`carousel-item ${index === 0 ? "active" : ""}`}
            >
              <img
                className="d-block mx-auto img-fluid"
                alt={`${photo}`}
                src={`${SERVER_IMAGE_URL}/${photo}`}
                style={{maxHeight: "50vh", minHeight: "50vh"}}
                // onClick={}
              />
            </div>
          );
        })}
      </div>
      <a
        className="carousel-control-prev"
        role="button"
        href="#carouselExampleIndicators"
        data-slide="prev"
      >
        <span className="carousel-control-prev-icon" aria-hidden="true" />
        <span className="sr-only">Previous</span>
      </a>
      <a
        className="carousel-control-next"
        role="button"
        href="#carouselExampleIndicators"
        data-slide="next"
      >
        <span className="carousel-control-next-icon" aria-hidden="true" />
        <span className="sr-only">Next</span>
      </a>
    </div>
  );
};

export default PhotoCarousel;
