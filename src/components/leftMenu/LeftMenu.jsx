/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getCategoriesFromCookie,
  deleteCategory,
} from "../../actions/categoriesAction";
import { useEffect } from "react";
import { Plus, Trash } from "react-feather";
import CategoryModal from "../categoryModal/CategoryModal";
import useWindowSize from "../../helpers/useWindowSize";
import DeleteModal from "../deleteModal/DeleteModal";
import { searchItems } from "../../actions/itemActions";
import { addCategory } from "../../actions/categoriesAction";

const LeftMenu = () => {
  const dispatch = useDispatch();

  const { categories, isSignedIn, saving, saved } = useSelector((state) => {
    return {
      categories: state.categories.categories,
      loading: state.categories.loading,
      deleted: state.categories.deleted,
      loaded: state.categories.loaded,
      error: state.categories.error,
      isSignedIn: state.auth.isSignedIn,
      queryId: state.auth.queryId,
      token: state.auth.token,
      saving: state.categories.saving,
      saved: state.categories.saved,
    };
  });

  useEffect(() => {
    dispatch(getCategoriesFromCookie());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const [modal, openModal] = useState(false);

  const [deleteModal, openDeleteModal] = useState(false);
  const [deletingId, setDeletingId] = useState("");

  const toggleDeleteModal = () => {
    openDeleteModal(!deleteModal);
  };

  const closeDeleteModal = () => {
    setDeletingId("");
    openDeleteModal(false);
  };

  const toggleCategoryModal = () => {
    openModal(!modal);
  };
  const closeCategoryModal = () => {
    openModal(false);
  };
  const handleDeleteCategory = () => {
    dispatch(deleteCategory(deletingId));
    toggleDeleteModal();
  };

  const submitCategory = (name) => {
    dispatch(addCategory(name));
  };

  useEffect(() => {
    if (deletingId) {
      toggleDeleteModal();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [deletingId]);

  useEffect(() => {
    if (saved) {
      toggleCategoryModal();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [saved]);

  // 997 pixelin altında mobil görünüme geçiyor. bu yuzden bu pixel altına listeyi akordeon falan yapmak lazım
  const size = useWindowSize();
  const mobileCategoryListStyle =
    size.width < 997 ? { maxHeight: "220px", overflowY: "auto" } : null;
  return (
    <div className="sticky-top">
      <h1 className="my-4">Kategoriler</h1>
      <div>
        <input
          type="text"
          onChange={(e) => dispatch(searchItems(e.target.value))}
          className="form-control mb-2 border-top-0 border-left-0 border-right-0 "
          placeholder="Search.."
          aria-label="Username"
          aria-describedby="addon-wrapping"
        />
      </div>
      <div className="list-group">
        <a
          className="list-group-item list-group-item-dark bg-dark text-light"
          style={{ textDecoration: "none" }}
          href="/home"
        >
          Tüm kategoriler
        </a>
        <div style={mobileCategoryListStyle}>
          {categories.map((category, index) => {
            return (
              <div
                key={category._id}
                className="d-flex list-group-horizontal list-group-flush justify-content-between bg-dark"
              >
                <a
                  style={{ textDecoration: "none" }}
                  className="list-group-item bg-dark text-light flex-grow-1 rounded-0"
                  href={`/home?category=${category._id}`}
                >
                  <div className="">{category.name}</div>
                </a>
                {isSignedIn ? (
                  <button
                    onClick={() => setDeletingId(category._id)}
                    className="bg-dark rounded-0 list-group-item"
                  >
                    <Trash color="red" size={16} />
                  </button>
                ) : null}
              </div>
            );
          })}
        </div>
        {isSignedIn ? (
          <button
            style={{ textDecoration: "none" }}
            className="list-group-item bg-dark text-info"
            onClick={toggleCategoryModal}
          >
            <span className="">
              Yeni Kategori Ekle{" "}
              <span>
                <Plus size={20} />
              </span>{" "}
            </span>
            {saving ? (
              <div className="spinner-border text-secondary" role="status">
                <span className="sr-only">Loading...</span>
              </div>
            ) : null}
          </button>
        ) : null}
      </div>
      {modal ? (
        <CategoryModal
          open={modal}
          onSubmit={submitCategory}
          closeCategoryModal={closeCategoryModal}
          toggle={toggleCategoryModal}
        />
      ) : null}
      <DeleteModal
        open={deleteModal}
        toggle={toggleDeleteModal}
        deleteConfirmed={handleDeleteCategory}
        closeDeleteModal={closeDeleteModal}
        body="Kategorinin tüm ilanları silinecektir. Bu kategoriyi"
      />
    </div>
  );
};

export default LeftMenu;
