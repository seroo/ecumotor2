import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Modal, ModalBody, ModalFooter } from "reactstrap";

const CategoryModal = (props) => {
  const [categoryName, setCategoryName] = useState("");

  const handleCategoryName = (e) => {
    setCategoryName(e.target.value);
  };

  const { error } = useSelector((state) => {
    return {
      queryId: state.auth.queryId,
      token: state.auth.token,
      saving: state.categories.saving,
      saved: state.categories.saved,
      error: state.categories.error,
    };
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    if (categoryName.length > 0) {
      props.onSubmit(categoryName);
    } else {
      setAlert(true);
    }
  };


  const [alert, setAlert] = useState(false);

  const alertComponent = (
    <div className="alert alert-danger" role="alert">
      Kategori adı giriniz..
    </div>
  );

  return (
    <div>
      <Modal isOpen={props.open} toggle={props.toggle}>
        <ModalBody>
          <form>
            <div className="form-group">
              <label htmlFor="exampleFormControlInput1">
                Kategori Adı<sup style={{ color: "red" }}>*</sup>
              </label>
              <input
                maxLength={20}
                className="form-control"
                id="exampleFormControlInput1"
                type="text"
                name="name"
                required
                autoFocus
                onChange={handleCategoryName}
                placeholder={"Kategori adı"}
              />
            </div>
          </form>
          {alert === true ? alertComponent : null}
        </ModalBody>
        <ModalFooter>
          

          {
            error ? <div class="alert alert-danger" role="alert">
            {error}
          </div> : null
          }

          <button onClick={handleSubmit} className="btn btn-dark btn-sm ml-1">
            Ekle
          </button>
          <button
            type="submit"
            className="btn btn-secondary btn-sm ml-1"
            onClick={props.toggle}
          >
            İptal
          </button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default CategoryModal;
