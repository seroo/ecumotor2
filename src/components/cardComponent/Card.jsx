/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { SERVER_IMAGE_URL } from '../../actions/actionConstants'
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';


const format = require('date-format')


const Card = (props) => {
  const history = useHistory();

    const { photos, header, description, price, category, createdAt, _id, slug } = props.item
    // format date
    const createdDateInstance = new Date.prototype.constructor(Date.parse(createdAt))
    //  createdDateInstance);
    const formattedDate = format('dd.MM.yyyy',createdDateInstance)

    // get categoryname from store
    const { categories } = useSelector((state) => {

        return {
            categories: state.categories.categories,
        }
    })
    const categoryName = categories && categories.map((categoryInLoop, index) => {

        if (categoryInLoop._id === category) {
            //  categoryInLoop);
            return categoryInLoop.name
        } else {
            return ""
        }

    })
  return (
    <div className="col-lg-4 col-md-6 mb-4">
      <div className="card h-100">
        <a href="#">
          <img
            className="card-img-top"
            alt={`${header}`}
            src={`${SERVER_IMAGE_URL}/${photos[0]}`}
            style={{maxHeight: "150px"}}
            onClick={() => history.push(`/home/${_id}/${slug}`)}
          />
        </a>
        <div className="card-body">
          <h4 className="card-title">
            <a className="text-secondary" style={{textDecoration: "none"}} href={`/home/${_id}/${slug}`}>{header}</a>
          </h4>
          <h5 className="text-body">{Intl.NumberFormat('tr').format(price)},-TL</h5>
          <p className="card-text text-body">
            {description}
          </p>
        </div>
        <div className="card-footer">
          <div><small className="text-muted">{categoryName}</small></div>
          <div><small className="text-muted">{formattedDate}</small></div>
        </div>
      </div>
    </div>
  );
};

export default Card;
