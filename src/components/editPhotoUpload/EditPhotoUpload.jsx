import React, { useState, useEffect } from "react";
import ThumbnailPanel from "../thumbnailPanel/ThumbnailPanel";
var _ = require("lodash");

const EditPhotoUpload = (props) => {
  const currentPhotosLength = props && props.photos && props.photos.length;
  let additional = parseInt(5 - currentPhotosLength);
  //   let photos = new Array(additional ? additional : null);

  //  currentPhotosLength);
  //  additional);
  //  props);

  const { handleFilesChange, deletePhoto } = props;

  const [filesArray, setFilesArray] = useState([]);

  const handleLocalChange = (index) => (e) => {
    // console.log(e.target.files);
    // console.log(index);
    // birleştirmek için array yapmak lazım ?
    const newFileArray = Array.from(e.target.files);
    // aynı fotoyu yüklememe
    // const alreadyIn = _.find(filesArray, (o) => o.name === newFileArray[0].name)

    let tmp = new Array(additional);

    tmp = filesArray.slice(); // new array returns

    tmp[index] = newFileArray[0];
    console.log(tmp);

    setFilesArray(tmp);
  };

  useEffect(() => {
    if (filesArray.length > 0) {
      handleFilesChange(_.compact(Array.from(filesArray)));
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filesArray]);

  return (
    <div className="container">
      <div>
        <ThumbnailPanel
          deletePhoto={deletePhoto}
          photos={props.photos && props.photos}
        />
      </div>
      <div>
        {[...Array(props && props.photos && additional)].map((e, index) => (
          <div key={index * 7} className="input-group mb-1">
            <div className="custom-file">
              <input
                className="custom-file-input"
                id="inputGroupFile01"
                aria-describedby="inputGroupFileAddon01"
                type="file"
                onChange={handleLocalChange(index)}
              />
              <label className="custom-file-label" htmlFor="inputGroupFile01">
                {filesArray && filesArray[index]
                  ? filesArray[index].name
                  : "Choose file"}
              </label>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default EditPhotoUpload;
