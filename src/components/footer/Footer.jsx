import React from "react";

const Footer = () => {
  const style = {
    position: "fixed",
    bottom: 0,
    width: "100%",
    height: "3rem",
  };

  return (
    <div >
      <footer style={style} className="py-3 bg-dark mt-3">
        <div className="container">
          <p className="m-0 text-center text-white">Copyright © ECUMOTO 2020</p>
        </div>
        {/* /.container */}
      </footer>
    </div>
  );
};

export default Footer;
