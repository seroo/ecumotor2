import React from "react";
import { Modal, ModalBody, ModalFooter, Button } from "reactstrap";

const DeleteModal = (props) => {
  
  return (
    <div>
      <Modal isOpen={props.open} toggle={props.toggle}>
        <ModalBody>{props.body} silmek istediğinize emin misiniz?</ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={props.deleteConfirmed}>
            Sil
          </Button>{" "}
          <button
            className="btn btn-secondary"
            onClick={
              props.closeDeleteModal ? props.closeDeleteModal : props.toggle
            }
          >
            İptal
          </button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default DeleteModal;
