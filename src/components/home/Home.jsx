import React, { useEffect } from "react";
import LeftMenu from "../leftMenu/LeftMenu";
import HomeCarousel from "../homeCarousel/HomeCarousel";
import CardsSection from "../cardsSection/CardsSection";
import Cookies from "js-cookie";
import { useSelector, useDispatch } from "react-redux";
import { setCookieUser, getUserDetails } from "../../actions/authActions";
import { getContactsFromSessionStorage } from "../../actions/contactActions";
import { Helmet } from "react-helmet";

const Home = () => {
  const { isSignedIn } = useSelector((state) => {
    return {
      isSignedIn: state.auth.isSignedIn,
    };
  });
  const dispatch = useDispatch();

  function setCookie() {
    dispatch(setCookieUser());
  }
  function getUser() {
    dispatch(getUserDetails(Cookies.get("queryId")));
  }
  
  function getContactFromSession() {
    if (isSignedIn) {
      dispatch(getContactsFromSessionStorage());
    }
  }
  useEffect(() => {
    if (sessionStorage.getItem("user")) {
      setCookie();
    }
    if (!isSignedIn && Cookies.get("token") && Cookies.get("queryId")) {
      getUser();
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isSignedIn]);

  useEffect(() => {
      //  BURADA SESSIONSTORAGE TAN SORGULASAM MI
      getContactFromSession();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <div>
      <Helmet>
        <title>Ecumoto | İlanlar</title>
        <meta name="description" content="Motosiklet yedek parçalarında güvenilir adres" />
        <meta name="keywords" content="motosiklet, motorsiklet, motor yedek parça, motor, yedek, parça" />
      </Helmet>
      {/* Page Content */}
      <div className="container">
        <div className="row">
          <div className="col-lg-3">
            <LeftMenu />
          </div>
          {/* /.col-lg-3 */}
          <div className="col-lg-9">
            <div className="mt-3 ml-1">
              <h3>Son 5 İlan..</h3>
            </div>
            <HomeCarousel />

            <div className="">
              <CardsSection />
            </div>
            {/* /.row */}
          </div>
          {/* /.col-lg-9 */}
        </div>
        {/* /.row */}
      </div>
      {/* /.container */}
    </div>
  );
};

export default Home;
