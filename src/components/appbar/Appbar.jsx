/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect } from "react";
import { PlusCircle, LogOut, User } from "react-feather";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import Cookies from 'js-cookie';
import { setCookieUser, getUserDetails, signOut } from '../../actions/authActions';
import ecu from './ecu.png'

const Appbar = (props) => {
  //  "NAVBAR RENDER");
  const history = useHistory();
  const dispatch = useDispatch();
  const { isSignedIn } = useSelector((state) => {
    return {
      isSignedIn: state.auth.isSignedIn,
    };
  });

  const pushTo = (to) => {
    history.push(to)
  }


  function setCookie() {
    dispatch(setCookieUser());
  }
  function getUser() {
    dispatch(getUserDetails(Cookies.get("queryId")));
  }
  useEffect(() => {
    if (sessionStorage.getItem("user")) {
      setCookie();
    } else {
      if (!isSignedIn && Cookies.get("token") && Cookies.get("queryId")) {
        getUser();
      }
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function logOut() {
    dispatch(signOut())
  }

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div className="container">
          <a className="navbar-brand" href="/">
            <img src={ecu} alt="EcuMoto" style={{minWidth: "100px", maxHeight: "50px"}}/>
          </a>
          {isSignedIn ? (
            <button
              onClick={() => pushTo("/new")}
              className="btn btn-sm btn-dark"
            >
              <span className="mr-1">
                <PlusCircle size={16} />
              </span>
              YENİ İLAN
            </button>
          ) : null}
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarResponsive"
            aria-controls="navbarResponsive"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarResponsive">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item active">
                <a className="nav-link" href="/">
                  Ana Sayfa
                  <span className="sr-only">(current)</span>
                </a>
              </li>
              {/* <li className="nav-item">
                <a className="nav-link" href="/">
                  About
                </a>
              </li> */}
              {isSignedIn ? <li className="nav-item">
                <a className="nav-link" href="/contact" onClick={() => pushTo("/contact")}  >
                  İletişim
                </a>
              </li> : null}
              {isSignedIn ? 
              <li className="nav-item" >
                <a className="nav-link" href="/me" >
                  <span ><User size={18} /></span>
                </a>
              </li> : null}
              {isSignedIn ? 
              <li className="nav-item">
                <a onClick={logOut} className="nav-link" href="#">
                  <span ><LogOut size={18} /></span>
                </a>
              </li> : null}
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Appbar;
