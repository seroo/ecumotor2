import React from "react";
import Card from "../cardComponent/Card";
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import { getItems, getItemsByCategory } from "../../actions/itemActions";
import { getCategoryName } from "../../helpers/helpers";
import { useLocation } from "react-router-dom";

const CardsSection = () => {
  //  "CARD SECTION RENDER");
  const dispatch = useDispatch();
  function useQuery() {
    return new URLSearchParams(useLocation().search);
  }
  const query = useQuery();

  const { items, categories, loading, loaded } = useSelector((state) => {
    //  state);

    return {
      items: state.getItems.items,
      categories: state.categories.categories,
      loading: state.getItems.loading,
      loaded: state.getItems.loaded,
      updating: state.getItems.updating,
      updated: state.getItems.updated,
      error: state.getItems.error,
    };
  });

  const categoryId = query.get("category");

  const categoryName = getCategoryName(categories, categoryId);

  //  categoryName);

  useEffect(() => {
    if (query.get("category") === null) {
      dispatch(getItems(""));
    } else {
      dispatch(getItemsByCategory(query.get("category")));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="pb-5">
      <div>
        <h4>
          {categoryName === null
            ? "TÜM KATEGORİLERDEKİ"
            : categoryName.toUpperCase() + " KATEGORİSİNDEKİ"}{" "}
          İLANLAR
        </h4>
        {loaded && items.length === 0 ? (
          <div className="mb-4">
            <div class="alert alert-primary" role="alert">
              Kategoride ilan bulunmamaktadır.
            </div>
            <img
              className="mt-2"
              src={require(`./empty${Math.floor(Math.random() * 4) + 1}.jpg`)}
              style={{ maxWidth: "500px" }}
              alt="empty"
            />
          </div>
        ) : null}
      </div>
      {!loading ? (
        <div className="row">
          {items.map((item, index) => {
            return <Card item={item} key={item._id} />;
          })}
        </div>
      ) : (
        <div
          className="spinner-border"
          style={{ width: "5rem", height: "5rem" }}
          role="status"
        >
          <span className="sr-only">Loading...</span>
        </div>
      )}
    </div>
  );
};

export default CardsSection;
