import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import LeftMenu from "../leftMenu/LeftMenu";
import { signIn } from "../../actions/authActions";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

const Login = () => {
  // console.log("LOGIN RENDER");
  const history = useHistory();
  const dispatch = useDispatch();
  // bunu böyle yapmazsam her seferinde hata verdi
  function pushSomeWhere(to) {
    history.push(to);
  }
  const { register, errors, handleSubmit } = useForm();

  const { loaded, } = useSelector((state) => {
    return {
      loading: state.auth.loading,
      loaded: state.auth.loaded,
      error: state.auth.error,
      isSignedIn: state.auth.isSignedIn,
    };
  });

  useEffect(() => {
    if (loaded) {
      pushSomeWhere("/");
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loaded]);

  const formSubmit = (e) => {
    // console.log(e);
    dispatch(signIn(e));
  };

  return (
    <div className="container mb-4">
      <div className="row">
        <div className="col-lg-3">
          <LeftMenu />
        </div>
        {/* /.col-lg-3 */}
        <form onSubmit={handleSubmit(formSubmit)} className="col-lg-9">
          <div className="card mt-4">
            {/* form start */}
            <div className="card-body">
              <div className="form-group">
                <label htmlFor="exampleFormControlInput1">Email</label>
                <input
                  ref={register({ required: true })}
                  className="form-control"
                  id="exampleFormControlInput1"
                  type="email"
                  name="email"
                  // value={formState.header}
                  // onChange={handleFormStateChange}
                  placeholder={"Email"}
                />
                {errors.email && (
                  <div className="alert alert-danger" role="alert">
                    Zorunlu alan
                  </div>
                )}
              </div>
              <div className="form-group">
                <label htmlFor="exampleFormControlInput1">Şifre</label>
                <input
                  ref={register({ required: true })}
                  className="form-control"
                  id="exampleFormControlInput1"
                  type="password"
                  name="password"
                  // value={formState.price}
                  // onChange={handleFormStateChange}
                  placeholder={"password"}
                />
                {errors.password && (
                  <div className="alert alert-danger" role="alert">
                    Zorunlu alan
                  </div>
                )}
              </div>
              <div>
                <button
                  onClick={() => pushSomeWhere("/")}
                  type="button"
                  className="btn btn-danger btn-sm"
                  style={{ width: "70px" }}
                >
                  İptal
                </button>
                <button
                  type="submit"
                  className="btn btn-secondary btn-sm ml-1"
                  style={{ width: "70px" }}
                >
                  Submit
                </button>
                {/* {error && (
                    <div className="alert alert-danger mt-1" role="alert">
                      {error.response &&
                        error.response.data &&
                        error.response.data.error &&
                        error.response.data.error}
                    </div>
                  )} */}
              </div>
            </div>
            {/* form end */}
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
