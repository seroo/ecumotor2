/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState } from "react";
import LeftMenu from "../leftMenu/LeftMenu";
import PhotoCarousel from "../photoCarousel/PhotoCarousel";
import { useRouteMatch, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getItemById, deleteItemById } from "../../actions/itemActions";
import { useEffect } from "react";
import DeleteModal from "../deleteModal/DeleteModal";
import { setCookieUser, getUserDetails } from "../../actions/authActions";
import Cookies from "js-cookie";
import { Helmet } from "react-helmet";
import ContactCard from "../contactCard/ContactCard";

const format = require("date-format");

const AdDetailPage = () => {
  //  "AD DETAIL RENDER");
  const match = useRouteMatch();
  //  match);
  const history = useHistory();

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getItemById(match.params.id));
  }, []);
  const {
    itemById,
    itemByIdLoading,
    error,
    isSignedIn,
    deleted,
  } = useSelector((state) => {
    return {
      itemById: state.getItems.itemById,
      itemByIdLoading: state.getItems.itemByIdLoading,
      loaded: state.getItems.itemByIdLoaded,
      deleted: state.getItems.deleted,
      error: state.getItems.itemByIdError,
      categories: state.categories.categories,
      isSignedIn: state.auth.isSignedIn,
    };
  });

  const pushHome = () => {
    history.push("/home");
  };
  useEffect(() => {
    if (deleted) {
      pushHome();
    }
  }, [deleted]);

  function setCookie() {
    dispatch(setCookieUser());
  }
  function getUser() {
    dispatch(getUserDetails(Cookies.get("queryId")));
  }
  useEffect(() => {
    if (sessionStorage.getItem("user")) {
      setCookie();
    } else {
      if (!isSignedIn && Cookies.get("token") && Cookies.get("queryId")) {
        getUser();
      }
    }
  }, []);

  // format date
  const createdDateInstance = new Date.prototype.constructor(
    Date.parse(itemById && itemById.createdAt)
  );
  //  createdDateInstance);
  const formattedDate = format("dd.MM.yyyy", createdDateInstance);

  const [deleteModal, toggleDeleteModal] = useState(false);
  const toggleDeleteItemModal = () => {
    toggleDeleteModal(!deleteModal);
  };
  const delItem = () => dispatch(deleteItemById(itemById._id));
  const deleteItemConfirmed = () => {
    //  "DELETE ITEM");
    toggleDeleteItemModal(false);
    delItem();
  };

  if (!itemByIdLoading) {
    return (
      <div className="container mb-4 pb-5">
        <Helmet>
          <title>
            Ecumoto | {`${itemById && itemById.header ? itemById.header : ""}`}
          </title>
          <meta
            name="description"
            content="Motosiklet yedek parçalarında güvenilir adres"
          />
          <meta
            name="keywords"
            content={` ${
              itemById &&
              itemById.category && itemById.category.name +
                ", motosiklet, motorsiklet, motor yedek parça, motor, yedek, parça"
            }`}
          />
        </Helmet>
        <div className="row">
          <div className="col-lg-3">
            <LeftMenu />
          </div>
          {/* /.col-lg-3 */}
          {error ? (
            <div>İlana Ulaşılamadı</div>
          ) : (
            <div className="col-lg-9">
              <div className="card mt-4">
                <PhotoCarousel photos={itemById && itemById.photos} />
                <div className="card-body">
                  <h3 className="card-title">{itemById && itemById.header}</h3>
                  <h4>
                    {itemById &&
                      itemById.price &&
                      Intl.NumberFormat("tr").format(itemById.price)}
                    ,-TL
                  </h4>
                  <p className="card-text">
                    {itemById && itemById.description}
                  </p>
                  <div className="text-info">İlan Tarihi: {formattedDate}</div>

                  <div className="row text-danger d-flex justify-content-between">
                    <div className="ml-3">Kategori: {itemById && itemById.category && itemById.category.name}</div>
                    {isSignedIn ? (
                      <div
                        className="btn-group"
                        role="group"
                        aria-label="Basic example"
                      >
                        <button
                          onClick={toggleDeleteItemModal}
                          type="button"
                          className="btn btn-danger btn-sm"
                          style={{ width: "70px" }}
                        >
                          Delete
                        </button>
                        <button
                          onClick={() => history.push(`/edit/${itemById._id}`)}
                          type="button"
                          className="btn btn-secondary btn-sm"
                          style={{ width: "70px" }}
                        >
                          Edit
                        </button>
                      </div>
                    ) : null}
                  </div>
                  {itemById ? (
                    <div className="mt-4">İletişim:
                      <ContactCard
                        showButtons={false}
                        contact={itemById.contact}
                      />
                    </div>
                  ) : null}
                </div>
              </div>
              {deleteModal ? <DeleteModal
                open={deleteModal}
                body={"İlanı"}
                toggle={toggleDeleteItemModal}
                deleteConfirmed={deleteItemConfirmed}
              /> : null}
            </div>
          )}
        </div>
      </div>
    );
  } else {
    return (
      <div
        style={{ width: "5rem", height: "5rem" }}
        className=" spinner-border text-secondary"
        role="status"
      >
        <span className="sr-only">Loading...</span>
      </div>
    );
  }
};

export default AdDetailPage;
