import React, { useEffect } from "react";
import LeftMenu from "../leftMenu/LeftMenu";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import {
  getUserDetailsFromServer,
  updateUserDetails,
} from "../../actions/authActions";
import { useHistory } from "react-router-dom";

const Profile = () => {
  const dispatch = useDispatch();
  const history = useHistory();

const pushHome = () => {
    history.replace("/home")
  }
  const { register, errors, handleSubmit, getValues, reset } = useForm();

  const formSubmit = (data, e) => {
    const { name, email, oldPassword, newPassword } = data;

    const details = {
      name,
      email,
      oldPassword,
      newPassword,
    };

    dispatch(updateUserDetails(details));
  };

  useEffect(() => {
    dispatch(getUserDetailsFromServer());
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const { name, email, userUpdated, userUpdating } = useSelector((state) => {
    return {
      name: state.auth.user && state.auth.user.name,
      email: state.auth.user && state.auth.user.email,
      userUpdated: state.auth.userUpdated,
      userUpdating: state.auth.userUpdating,
    };
  });

  useEffect(() => {
    reset();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userUpdated]);

  return (
    <div className="container mb-4 pb-5">
      <div className="row">
        <div className="col-lg-3">
          <LeftMenu />
        </div>
        {/* /.col-lg-3 */}
        <form onSubmit={handleSubmit(formSubmit)} className="col-lg-9">
          <div className="card mt-4">
            {/* form start */}
            <div className="card-body">
              <div className="form-group">
                <label htmlFor="exampleFormControlInput1">İsim - Soyisim</label>
                <input
                  ref={register({ required: true })}
                  className="form-control"
                  id="exampleFormControlInput1"
                  type="text"
                  name="name"
                  defaultValue={name && name}
                  // value={formState.name}
                  // onChange={handleFormStateChange}
                  placeholder={"İsim"}
                />
                {errors.name && (
                  <div className="alert alert-danger" role="alert">
                    Zorunlu alan
                  </div>
                )}
              </div>
              <div className="form-group">
                <label htmlFor="exampleFormControlInput1">Email</label>
                <input
                  ref={register({ required: true })}
                  className="form-control"
                  id="exampleFormControlInput1"
                  type="email"
                  name="email"
                  defaultValue={email && email}
                  // value={formState.email}
                  // onChange={handleFormStateChange}
                  placeholder={"Email"}
                />
                {errors.email && (
                  <div className="alert alert-danger" role="alert">
                    Zorunlu alan
                  </div>
                )}
              </div>

              <div className="form-group">
                <label htmlFor="exampleFormControlInput1">Eski Şifre</label>
                <input
                  ref={register({ required: true })}
                  className="form-control"
                  id="exampleFormControlInput1"
                  type="password"
                  name="oldPassword"
                  // value={formState.oldPassword}
                  // onChange={handleFormStateChange}
                  placeholder={"Eski şifre"}
                />
                {errors.oldPassword && (
                  <div className="alert alert-danger" role="alert">
                    Zorunlu alan
                  </div>
                )}
              </div>

              <div className="form-group">
                <label htmlFor="exampleFormControlInput1">Yeni Şifre</label>
                <input
                  ref={register()}
                  className="form-control"
                  id="exampleFormControlInput1"
                  type="password"
                  name="newPassword"
                  // value={formState.newPassword}
                  // onChange={handleFormStateChange}
                  placeholder={"Yeni Şifre"}
                />
                {/* {errors.password && (
                  <div className="alert alert-danger" role="alert">
                    Zorunlu alan
                  </div>
                )} */}
              </div>

              <div className="form-group">
                <label htmlFor="exampleFormControlInput1">
                  Yeni Şifre Tekrar
                </label>
                <input
                  ref={register({
                    validate: (value) => {
                      return value === getValues("newPassword").newPassword;
                    },
                  })}
                  className="form-control"
                  id="exampleFormControlInput1"
                  type="password"
                  name="password2"
                  // value={formState.password2}
                  // onChange={handleFormStateChange}
                  placeholder={"Yeni Şifre Tekrar"}
                />
                {errors.password2 && (
                  <div className="alert alert-danger" role="alert">
                    Şifreler uyuşmuyor..
                  </div>
                )}
              </div>

              {userUpdated ? (
                <div className="alert alert-info" role="alert">
                  Kullanıcı bilgileri güncellendi
                </div>
              ) : null}

              {userUpdating ? (
                <div className="spinner-border text-secondary" role="status">
                  <span className="sr-only">Loading...</span>
                </div>
              ) : null}

              <div>
                <button
                  onClick={() => pushHome("/")}
                  type="button"
                  className="btn btn-danger btn-sm"
                  style={{ width: "70px" }}
                >
                  İptal
                </button>
                <button
                  type="submit"
                  className="btn btn-secondary btn-sm ml-1"
                  style={{ width: "70px" }}
                >
                  Submit
                </button>
                {/* {error && (
                    <div className="alert alert-danger mt-1" role="alert">
                      {error.response &&
                        error.response.data &&
                        error.response.data.error &&
                        error.response.data.error}
                    </div>
                  )} */}
              </div>
            </div>
            {/* form end */}
          </div>
        </form>
      </div>
    </div>
  );
};

export default Profile;
