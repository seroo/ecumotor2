import React, { useEffect } from "react";
import LeftMenu from "../leftMenu/LeftMenu";
import { Info, PlusCircle } from "react-feather";
import ContactCard from "../contactCard/ContactCard";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { getContacts, createContact } from "../../actions/contactActions";
import { Modal } from "reactstrap";
import NewContactModal from "../contactCard/NewContactModal";


const Contact = (props) => {
  const dispatch = useDispatch();

  const { contacts, saved } = useSelector((state) => {
    return {
      saved: state.contacts.saving,
      deleted: state.contacts.saving,
      contacts: state.contacts.contacts,
    };
  });

  const submitForm = (form) => {
    dispatch(createContact(form));
  };

  const [modal, toggleModal] = useState(false);

  const toggleContactModal = () => {
    toggleModal(!modal);
  };

  const closeContactModal = () => {
    toggleModal(false);
  };

  useEffect(() => {
    if (saved) {
      closeContactModal();
    }
  }, [saved]);

  useEffect(() => {
    dispatch(getContacts());
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="pb-5">
      <div className="container">
        <div className="row">
          <div className="col-lg-3">
            <LeftMenu />
          </div>
          <div
            className="col-md-6 mt-4"
            style={{ paddingLeft: "20px", paddingRight: "20px" }}
          >
            <div className="d-flex mt-2">
              <legend className="">
                <span>
                  <Info size={20} />
                </span>{" "}
                İletişim bilgilerimiz..
              </legend>
              <button
                type="button"
                onClick={toggleContactModal}
                className="btn btn-dark mb-3 "
              >
                Yeni Contact Bilgisi Ekle
                <span className="ml-2">
                  <PlusCircle size={16} />
                </span>{" "}
              </button>
            </div>
            {contacts &&
              contacts.map((contact, index) => {
                return (
                  <ContactCard
                    showButtons={true}
                    key={contact._id}
                    contact={contact}
                  />
                );
              })}
          </div>

          <div>
            <Modal isOpen={modal}>
              <NewContactModal
                onSubmit={submitForm}
                toggle={toggleContactModal}
                closeModal={closeContactModal}
              />
            </Modal>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Contact;
