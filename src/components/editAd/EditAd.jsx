/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import LeftMenu from "../leftMenu/LeftMenu";
import EditPhotoUpload from "../editPhotoUpload/EditPhotoUpload";
import { useRouteMatch, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  getItemById,
  updateItemById,
  deletePhotoById,
  deleteItemById,
} from "../../actions/itemActions";
import { useEffect } from "react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import DeleteModal from "../deleteModal/DeleteModal";
import { setCookieUser, getUserDetails } from "../../actions/authActions";
import { getContactsFromSessionStorage } from "../../actions/contactActions";
import Cookies from "js-cookie";

const format = require("date-format");

const EditAd = () => {
  const match = useRouteMatch();
  const history = useHistory();

  const dispatch = useDispatch();

  const { register, errors, handleSubmit } = useForm();

  async function fetchItem() {
    await dispatch(getItemById(match.params.id));
    // await dispatch(getItemById(123));
  }
  // bunu böyle yapmazsam ve direkt history push edersem her seferinde hata verdi
  useEffect(() => {
    fetchItem();
  }, []);

  const {
    itemById,
    categories,
    error,
    updated,
    photoDeleted,
    deleted,
    isSignedIn,
    updating,
    contacts,
  } = useSelector((state) => {
    return {
      itemById: state.getItems.itemById,
      updated: state.getItems.updated,
      updating: state.getItems.updating,
      categories: state.categories.categories,
      photoDeleted: state.getItems.photoDeleted,
      deleted: state.getItems.deleted,
      isSignedIn: state.auth.isSignedIn,
      error: state.getItems.itemByIdError || state.getItems.error,
      contacts: state.contacts.contacts,
    };
  });

  function setCookie() {
    dispatch(setCookieUser());
  }
  function getUser() {
    dispatch(getUserDetails(Cookies.get("queryId")));
  }
  useEffect(() => {
    if (sessionStorage.getItem("user")) {
      setCookie();
    } else {
      if (!isSignedIn && Cookies.get("token") && Cookies.get("queryId")) {
        getUser();
      }
    }
    dispatch(getContactsFromSessionStorage());
  }, []);

  // bunu böyle yapmazsam her seferinde hata verdi
  function pushBack() {
    history.push("/");
  }
  useEffect(() => {
    if (updated) {
      pushBack();
    }
  }, [updated]);

  // format date
  const createdDateInstance = new Date.prototype.constructor(
    Date.parse(itemById && itemById.createdAt)
  );
  const formattedDate = format("dd.MM.yyyy", createdDateInstance);

  if (itemById) {
    // surekli itembyid null hatasi vardi
    var {
      header,
      price,
      description,
      category,
      photos,
      _id,
      contact,
    } = itemById;
  }

  const [formState, setFormState] = useState({
    header: header,
    price: price,
    description: description,
    category: category,
    contact: contact,
  });

  const [files, setFiles] = useState([]);

  const handleFilesChange = (files) => {
    setFiles(files);
  };

  useEffect(() => {
    setFormState((prevState) => {
      return {
        ...prevState,
        header: itemById && itemById.header,
        price: itemById && itemById.price,
        description: itemById && itemById.description,
        category: itemById && itemById.category && itemById.category._id,
        contact: itemById && itemById.contact && itemById.contact._id,
      };
    });
  }, [itemById]);

  const handleFormStateChange = (e) => {
    const { name, value } = e.target;
    // console.log(name);
    // console.log(value);

    setFormState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const [deleteModal, toggleDeleteModal] = useState(false);
  const toggleDeleteItemModal = () => {
    toggleDeleteModal(!deleteModal);
  };
  const deleteItemConfirmed = () => {
    dispatch(deleteItemById(_id));
  };

  useEffect(() => {
    if (deleted) {
      pushBack();
    }
  }, [deleted]);

  const delPhoto = (photoName) => {
    dispatch(deletePhotoById(_id, photoName));
  };

  useEffect(() => {
    if (photoDeleted) {
      fetchItem();
    }
  }, [photoDeleted]);

  const handleFormSubmit = (e) => {
    let updatedForm = Object.assign({}, formState);

    if (updatedForm.contact === null && contacts.length > 0) {
      updatedForm.contact = contacts[0]._id;
    }
    if (updatedForm.category === null && categories.length > 0) {
      updatedForm.category = categories[0]._id;
    }

    if (files) {
      updatedForm.files = files;
    }
    updatedForm.queryId = Cookies.get("queryId");
    dispatch(updateItemById(_id, updatedForm));
  };

  return (
    <div className="container mb-4 pb-5">
      <div className="row">
        <div className="col-lg-3">
          <LeftMenu />
        </div>
        {/* /.col-lg-3 */}

        <div className="col-lg-9">
          <div className="card mt-4 border">
            <EditPhotoUpload
              deletePhoto={delPhoto}
              photos={photos}
              handleFilesChange={handleFilesChange}
            />

            {/* form start */}
            <div className="card-body">
              <form onSubmit={handleSubmit(handleFormSubmit)}>
                <div className="form-group">
                  <label htmlFor="exampleFormControlInput1">
                    İlan Başlığı<sup style={{ color: "red" }}>*</sup>
                  </label>
                  <input
                    ref={register({ required: true })}
                    className="form-control"
                    id="exampleFormControlInput1"
                    type="text"
                    name="header"
                    value={formState.header}
                    onChange={handleFormStateChange}
                    placeholder={"ilan başlığı"}
                  />
                  {errors.header && (
                    <div className="alert alert-danger" role="alert">
                      Zorunlu alan
                    </div>
                  )}
                </div>
                <div className="form-group">
                  <label htmlFor="exampleFormControlInput1">
                    Fiyat<sup style={{ color: "red" }}>*</sup>
                  </label>
                  <input
                    ref={register({ required: true })}
                    className="form-control"
                    id="exampleFormControlInput1"
                    type="number"
                    name="price"
                    value={formState.price}
                    onChange={handleFormStateChange}
                    placeholder={"0.00"}
                  />
                  {console.log(errors)}
                  {errors.price && (
                    <div className="alert alert-danger" role="alert">
                      Zorunlu alan
                    </div>
                  )}
                </div>
                <div className="form-group">
                  <label htmlFor="exampleFormControlInput1">
                    Açıklama<sup style={{ color: "red" }}>*</sup>
                  </label>
                  <textarea
                    ref={register({ required: true })}
                    className="form-control"
                    id="exampleFormControlInput1"
                    rows="3"
                    name="description"
                    value={formState.description}
                    onChange={handleFormStateChange}
                    placeholder={"Açıklama..."}
                  />
                  {errors.description && (
                    <div className="alert alert-danger" role="alert">
                      Zorunlu alan
                    </div>
                  )}
                </div>

                <label>Kategori Seçimi</label>
                <div className="form-group">
                  <select
                    name="category"
                    onChange={handleFormStateChange}
                    class="custom-select"
                  >
                    {categories &&
                      categories.map((category, index) => {
                        let name = "";
                        itemById && itemById.category
                          ? (name = itemById.category.name)
                          : (name = "");
                        return (
                          <option
                            key={`${category._id}${index * 22}`}
                            selected={name === category.name}
                            value={category._id}
                          >
                            {category.name}
                          </option>
                        );
                      })}
                  </select>
                </div>
                <div className="form-group">
                  <label className="mt-2">İletişim Seçimi</label>
                  <select
                    onChange={handleFormStateChange}
                    name="contact"
                    class="custom-select"
                  >
                    {itemById &&
                      contacts.map((contact, index) => {
                        return (
                          <option
                            key={contact._id}
                            selected={
                              itemById.contact &&
                              itemById.contact.name &&
                              itemById.contact.name === contact.name
                                ? true
                                : false
                            }
                            value={contact._id}
                          >
                            {contact.name}
                          </option>
                        );
                      })}
                  </select>
                </div>
                <div className="text-info mb-2">
                  İlan Tarihi: {formattedDate}
                </div>

                <div>
                  <button
                    onClick={toggleDeleteItemModal}
                    type="button"
                    className="btn btn-danger btn-sm"
                    style={{ width: "70px" }}
                  >
                    Delete
                  </button>
                  <button
                    type="submit"
                    className="btn btn-secondary btn-sm ml-1"
                    style={{ width: "70px" }}
                  >
                    Update
                  </button>
                  {updating ? (
                    <div
                      className="ml-3 spinner-border text-secondary"
                      role="status"
                    >
                      <span className="sr-only">Loading...</span>
                    </div>
                  ) : null}
                  {error && (
                    <div className="alert alert-danger mt-1" role="alert">
                      {error.response &&
                        error.response.data &&
                        error.response.data.error &&
                        error.response.data.error}
                    </div>
                  )}
                </div>
              </form>
            </div>
            {/* form end */}
            <DeleteModal
              open={deleteModal}
              body={"İlanı"}
              toggle={toggleDeleteItemModal}
              deleteConfirmed={deleteItemConfirmed}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditAd;
