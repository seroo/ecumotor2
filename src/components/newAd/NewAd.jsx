/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import LeftMenu from "../leftMenu/LeftMenu";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { useHistory } from "react-router-dom";

import { useState } from "react";
import { useForm } from "react-hook-form";
import Dropzone from "react-dropzone-uploader";
import "react-dropzone-uploader/dist/styles.css";
import { Alert } from "reactstrap";
import { addItem } from "../../actions/itemActions";
import Cookies from "js-cookie";
import { getContactsFromSessionStorage } from "../../actions/contactActions";

const NewAd = () => {
  // console.log("NEW AD RENDER");
  const history = useHistory();
  const dispatch = useDispatch();

  const { register, errors, handleSubmit } = useForm();
  // console.log(watch('category'));
  // console.log(errors);
  
  useEffect(() => {
    dispatch(getContactsFromSessionStorage());
  }, [])
  
  const { saved, error, categories, contacts } = useSelector((state) => {
    return {
      saving: state.getItems.saving,
      saved: state.getItems.saved,
      error: state.getItems.error,
      categories: state.categories.categories,
      isSignedIn: state.auth.isSignedIn,
      contacts: state.contacts.contacts
    };
  });

  // bunu böyle yapmazsam her seferinde hata verdi
  function pushSomeWhere(to) {
    history.push(to);
  }
  useEffect(() => {
    if (saved) {
      pushSomeWhere("/");
    }
  }, [saved]);

  const [formState, setFormState] = useState({
    header: "",
    price: "",
    description: "",
    category: "",
    contact: ""
  });
  const [fileState, setFileState] = useState([]);

  const addFileToState = async (file) => {
    await setFileState(fileState.concat([file]));
  };

  const removeFileToState = async (file) => {
    let newArray = fileState.filter((fileInState) => {
      return fileInState !== file;
    });
    setFileState(newArray);
  };

  const handleFormStateChange = (e) => {
    // console.log(e.target.name);
    // console.log(e.target.value);

    const { name, value } = e.target;
    setFormState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  const [alertMessage, setAlertMessage] = useState("");
  // called every time a file's `status` changes
  const handleChangeStatus = ({ meta, file }, status) => {
    // console.log(status);
    if (status === "rejected_max_files") {
    }
    if (status === "error_file_size") {
    }
    if (status === "rejected_file_type") {
    }
    if (status === "done") {
      addFileToState(file);
    }
    if (status === "removed") {
      removeFileToState(file);
    }
  };


  const handleFormSubmit = (e) => {
    let form = new FormData();

    for (let index = 0; index < fileState.length; index++) {
      form.append("photos", fileState[index]);
    }

    form.append("header", e.header);
    form.append("price", e.price);
    form.append("description", e.description);
    form.append("category", formState.category);
    form.append("contact", formState.contact);
    form.append("queryId", Cookies.get("queryId"));

    // console.log(...form);

    dispatch(addItem(form));
  };

  console.log(error);
  

  return (
    <div className="container mb-4 pb-5">
      <div className="row">
        <div className="col-lg-3">
          <LeftMenu />
        </div>
        {
          <form onSubmit={handleSubmit(handleFormSubmit)} className="col-lg-9">
            <div className="card mt-4">
              {/* <PhotoUpload  /> */}
              <Dropzone
                ref={register}
                multiple
                name="files"
                submitButtonDisabled
                submitButtonContent=""
                autoUpload={false}
                inputContent={
                  "Fotoğrafları sürükleyin ya da seçmek için tiklayın"
                }
                onChangeStatus={handleChangeStatus}
                maxSizeBytes={(1024 * 1024) * 3}
                maxFiles={5}
                accept="image/*"
                canRestart={false}
              />
              {alertMessage ? (
                <Alert
                  color="danger"
                  isOpen={alertMessage.length > 0}
                  toggle={() => setAlertMessage("")}
                >
                  {alertMessage}
                </Alert>
              ) : null}
              {/* form start */}
              <div className="card-body">
                <div className="form-group">
                  <label htmlFor="exampleFormControlInput1">İlan Başlığı</label>
                  <input
                    ref={register({ required: true })}
                    className="form-control"
                    id="exampleFormControlInput1"
                    type="text"
                    name="header"
                    value={formState.header}
                    onChange={handleFormStateChange}
                    placeholder={"ilan başlığı"}
                  />
                  {errors.header && (
                    <div className="alert alert-danger" role="alert">
                      Zorunlu alan
                    </div>
                  )}
                </div>
                <div className="form-group">
                  <label htmlFor="exampleFormControlInput1">Fiyat</label>
                  <input
                    ref={register({ required: true })}
                    className="form-control"
                    id="exampleFormControlInput1"
                    type="number"
                    name="price"
                    value={formState.price}
                    onChange={handleFormStateChange}
                    placeholder={"0.00"}
                  />
                  {errors.price && (
                    <div className="alert alert-danger" role="alert">
                      Zorunlu alan
                    </div>
                  )}
                </div>
                <div className="form-group">
                  <label htmlFor="exampleFormControlInput1">Açıklama</label>
                  <textarea
                    ref={register({ required: true })}
                    className="form-control"
                    id="exampleFormControlInput1"
                    rows="3"
                    name="description"
                    value={formState.description}
                    onChange={handleFormStateChange}
                    placeholder={"Açıklama..."}
                  />
                  {errors.description && (
                    <div className="alert alert-danger" role="alert">
                      Zorunlu alan
                    </div>
                  )}
                </div>
                <div className="form-group">
                  <label htmlFor="exampleFormControlSelect2">
                    Kategori seçimi
                  </label>
                  <select
                  ref={register({ required: true })}
                    className="form-control"
                    id="exampleFormControlSelect1"
                    onChange={handleFormStateChange}
                    name="category"
                  >
                    <option selected={true} value={""} label={"Seçiniz."}>
                    </option>
                    {categories &&
                      categories.map((category, index) => {
                        return (
                          <option
                            key={category._id}
                            value={category._id}
                          >
                            {category.name}
                          </option>
                        );
                      })}
                    
                  </select>
                  {errors.category && (
                      <div className="alert alert-danger" role="alert">
                        Zorunlu alan
                      </div>
                    )}
                </div>
                <div className="form-group">
                  <label >İletişim Seçimi</label>
                  <select ref={register({ required: true })} required class="custom-select" name="contact" onChange={handleFormStateChange} >
                    <option selected >İletişim seçimi</option>
                      {
                        contacts && contacts.map((contact, index) => {
                          return <option key={contact._id} value={contact._id} >{contact.name}</option>
                        })
                      }
                  </select>
                  {errors.category && (
                      <div className="alert alert-danger" role="alert">
                        Zorunlu alan
                      </div>
                    )}
                </div>
                <div>
                  <button
                    onClick={() => pushSomeWhere("/")}
                    type="button"
                    className="btn btn-danger btn-sm"
                    style={{ width: "70px" }}
                  >
                    İptal
                  </button>
                  <button
                    type="submit"
                    className="btn btn-secondary btn-sm ml-1"
                    style={{ width: "70px" }}
                  >
                    Submit
                  </button>
                  {error && (
                    <div className="alert alert-danger mt-1" role="alert">
                      {error.response &&
                        error.response.data &&
                        error.response.data.error &&
                        String(error.response.data.error).startsWith("Cast") ? "İletişim Seçiniz" : 
                        error.response.data.error
                        }
                    </div>
                  )}
                </div>

                <div className="text-danger">
                </div>
              </div>
            </div>
          </form>
        }
      </div>
    </div>
  );
};

export default NewAd;
