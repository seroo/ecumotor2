import React from "react";
import { SERVER_IMAGE_URL } from "../../actions/actionConstants";
import { useSelector } from "react-redux";
import { useHistory } from 'react-router-dom';

const HomeCarousel = () => {
    const history = useHistory();

  const { items } = useSelector((state) => {
      
    return {
      items: state.getItems.items,
      loading: state.getItems.loading,
      loaded: state.getItems.loaded,
      error: state.getItems.error,
    };
  });

  let lastFiveProducts = [];

  if (items) {

    let length = items.length > 5 ? 5 : items.length
    

    for (let index = 0; index < length; index++) {
      lastFiveProducts.push(items[index])
      
    }
  }
  

  return (
    <div
      className="carousel slide my-4"
      id="carouselExampleIndicators"
      data-ride="carousel"
    >
      <ol className="carousel-indicators">
        {lastFiveProducts.map((item, index) => {
            return (
              <li
              key={index + (index * 2)}
                className={`${index === 0 ? "active" : ""}`}
                data-slide-to={index}
                data-target="#carouselExampleIndicators"
              />
            );
          })}
      </ol>

      <div className="carousel-inner" role="listbox">
        {lastFiveProducts.map((item, index) => {
             const photo = item.photos[0]
            return (
              <div
                key={`${index}${photo}`}
                className={`carousel-item ${index === 0 ? "active" : ""}`}
              >
                <img
                  className="d-block mx-auto img-fluid"
                  alt={`${photo}`}
                  src={`${SERVER_IMAGE_URL}/${photo}`}
                  style={{ maxHeight: "40vh", minHeight: "40vh" }}
                  onClick={() => history.push(`/home/${item._id}/${item.slug}`)}
                />
              </div>
            );
          })}
      </div>
      <a
        className="carousel-control-prev"
        role="button"
        href="#carouselExampleIndicators"
        data-slide="prev"
      >
        <span className="carousel-control-prev-icon" aria-hidden="true" />
        <span className="sr-only">Previous</span>
      </a>
      <a
        className="carousel-control-next"
        role="button"
        href="#carouselExampleIndicators"
        data-slide="next"
      >
        <span className="carousel-control-next-icon" aria-hidden="true" />
        <span className="sr-only">Next</span>
      </a>
    </div>
  );
};

export default HomeCarousel;
