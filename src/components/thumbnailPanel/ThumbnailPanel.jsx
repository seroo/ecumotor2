/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import { SERVER_IMAGE_URL } from "../../actions/actionConstants";
import { X } from "react-feather";
import DeleteModal from "../deleteModal/DeleteModal";
import { useState } from "react";



const ThumbnailPanel = (props) => {
  // console.log(adId);

  const [modal, setModal] = useState(false);
  const [photoName, setPhotoName] = useState("");

  const nameToPhoto = (photo) => setPhotoName(photo)

  const toggleModal = (photo) => {
    nameToPhoto(photo)
    setModal(!modal);
  
  }
  const deleteConfirmed = () => {
    props.deletePhoto(photoName)
    toggleModal("")
  };
  
  // console.log(photoName);
  

  return (
    <div className="row ml-1">
      {props &&
        props.photos &&
        props.photos.map((photo, index) => (
          <div key={index * 13}>
            <div className="col- my-1 mx-1">
              <img
                style={{ maxHeight: "15vh" }}
                src={`${SERVER_IMAGE_URL}/${photo}`}
                className="img-fluid"
                alt=""
              />
            </div>
            <div className="d-flex justify-content-center">
              <a
                onClick={() => {
                  toggleModal(photo);
                }}
                className="btn btn-sm btn-outline-light"
              >
                <span>
                  <X color={"red"} size={16} />
                </span>
              </a>
            </div>
          </div>
        ))}
      <DeleteModal
      body={"Fotoğrafı"}
        open={modal}
        toggle={toggleModal}
        deleteConfirmed={deleteConfirmed}
      />
    </div>
  );
};

export default ThumbnailPanel;
