import { createStore, applyMiddleware } from "redux";
import rootReducer from "../reducers/rootReducer";
import { composeWithDevTools } from "redux-devtools-extension";
import logger from "redux-logger";
import createSagaMiddleware from "redux-saga";
import { watchAuth, watchAds, watchCategories, watchContacts } from "../sagas/sagaWatchers";

export const configureStore = (preloadedState) => {
  const sagaMiddleware = createSagaMiddleware();

  let middlewares = null;
  if (process.env.REACT_APP_LOG) {
    middlewares = [logger, sagaMiddleware];
  } else {
    middlewares = [sagaMiddleware];
  }
  const middlewareEnhancer = applyMiddleware(...middlewares);

  const storeEnhancers = [middlewareEnhancer];

  const composedEnhancer = composeWithDevTools(...storeEnhancers);

  const store = createStore(rootReducer, preloadedState, composedEnhancer);
  sagaMiddleware.run(watchAuth);
  sagaMiddleware.run(watchAds);
  sagaMiddleware.run(watchCategories);
  sagaMiddleware.run(watchContacts);

  return store;
};
