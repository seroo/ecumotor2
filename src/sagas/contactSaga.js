import { put } from "redux-saga/effects";
import axios from "axios";
import * as actionTypes from "../actions/actionConstants";
const _ = require("lodash");
export function* getContactsSaga(action) {
  try {
    const response = yield axios.get(
      `${actionTypes.SERVER_URL}/api/v1/contacts`
    );

    if (response.data.data.length > 0) {
      yield sessionStorage.setItem(
        "contacts",
        JSON.stringify(response.data.data)
      );
    } else {
      yield sessionStorage.removeItem("contacts");
    }

    yield put({
      type: actionTypes.GET_CONTACTS_DONE,
      payload: response.data.data,
    });
  } catch (error) {
    console.log(error);

    yield put({
      type: actionTypes.GET_CONTACTS_ERROR,
      payload: error,
    });
  }
}

export function* getContactsFromSessionStorageSaga(action) {
  try {
    const contactsString = yield sessionStorage.getItem("contacts");
    const contactsJson = JSON.parse(contactsString);
    if (contactsJson) {
      yield put({
        type: actionTypes.GET_CONTACTS_DONE,
        payload: contactsJson,
      });
    } else {
      yield put({
        type: actionTypes.GET_CONTACTS_START,
      });
    }
  } catch (error) {
    yield put({
      type: actionTypes.GET_CONTACTS_START,
    });
  }
}

export function* createContactSaga(action) {
  // console.log(action);

  try {
    const response = yield axios.post(
      `${actionTypes.SERVER_URL}/api/v1/contacts`,
      action.contact,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${action.token}`,
        },
        params: { queryId: action.queryId },
      }
    );

    yield put({
      type: actionTypes.CREATE_CONTACT_DONE,
      payload: response.data.data,
    });

    yield put({
      type: actionTypes.GET_CONTACTS_START,
    });
  } catch (error) {
    yield put({
      type: actionTypes.CREATE_CONTACT_ERROR,
      error: error,
    });
  }
}

export function* updateContactSaga(action) {
  try {
    // eslint-disable-next-line no-unused-vars
    const response = yield axios.put(
      `${actionTypes.SERVER_URL}/api/v1/contacts/${action._id}`,
      action.updatedContact,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${action.token}`,
        },
        params: { queryId: action.queryId },
      }
    );

    yield put({
      type: actionTypes.UPDATE_CONTACT_DONE,
    });

    yield put({
      type: actionTypes.GET_CONTACTS_START,
    });
  } catch (error) {
    yield put({
      type: actionTypes.UPDATE_CONTACT_ERROR,
      payload: error,
    });
  }
}

export function* deleteContactSaga(action) {
  try {
    const response = yield axios.delete(
      `${actionTypes.SERVER_URL}/api/v1/contacts/${action._id}`,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${action.token}`,
        },
        params: { queryId: action.queryId },
      }
    );

    yield put({
      type: actionTypes.DELETE_CONTACT_DONE,
      payload: response.data.data,
    });
    yield put({
      type: actionTypes.GET_CONTACTS_START,
    });
  } catch (error) {
    yield put({
      type: actionTypes.DELETE_CONTACT_ERROR,
    });
  }
}
