import { takeEvery, takeLatest } from 'redux-saga/effects'

import * as actionTypes from '../actions/actionConstants';
import { signOutSaga, signInSaga, checkAuthStateSaga, setCookieUserSaga, signUpSaga, getUserDetailsSaga, getUserDetailsFromServerSaga, updateUserDetailsSaga } from './authSaga'
import { getItemsSaga, getItemByIdSaga, getItemsByCategorySaga, addItemSaga, updateItemSaga, deletePhotoByIdSaga, deleteItemById, searchItemsSaga } from './adSaga';
import { getCategoriesSaga, getCategoriesFromCookieSaga, addCategorySaga, deleteCategorySaga } from './categorySaga';
import { getContactsSaga, getContactsFromSessionStorageSaga, createContactSaga, deleteContactSaga, updateContactSaga } from './contactSaga';

export function* watchAuth() {
    yield takeEvery(actionTypes.SIGN_OUT_START, signOutSaga);
    yield takeEvery(actionTypes.SIGN_IN_START, signInSaga);
    yield takeEvery(actionTypes.CHECK_USER_SIGNED_IN_START, checkAuthStateSaga);
    yield takeEvery(actionTypes.SET_COOKIE_USER_START, setCookieUserSaga);
    yield takeEvery(actionTypes.SIGN_UP_START, signUpSaga);
    yield takeEvery(actionTypes.GET_USER_START, getUserDetailsSaga);
    yield takeEvery(actionTypes.GET_USER_FROM_SERVER_START, getUserDetailsFromServerSaga);
    yield takeLatest(actionTypes.UPDATE_USER_START, updateUserDetailsSaga);
}

export function* watchAds() {
    yield takeEvery(actionTypes.GET_ITEMS_START, getItemsSaga);
    yield takeLatest(actionTypes.SEARCH_ITEMS_START, searchItemsSaga);
    yield takeEvery(actionTypes.GET_ITEM_BY_ID_START, getItemByIdSaga);
    yield takeEvery(actionTypes.GET_ITEM_BY_CATEGORY_START, getItemsByCategorySaga);
    yield takeEvery(actionTypes.ADD_ITEM_START, addItemSaga);
    yield takeEvery(actionTypes.UPDATE_ITEM_BY_ID_START, updateItemSaga);
    yield takeLatest(actionTypes.DELETE_PHOTO_WITH_ID_START, deletePhotoByIdSaga);
    yield takeLatest(actionTypes.DELETE_ITEM_START, deleteItemById);
}

export function* watchCategories() {
    yield takeEvery(actionTypes.GET_CATEGORIES_START, getCategoriesSaga);
    yield takeEvery(actionTypes.GET_CATEGORIES_FROM_COOKIE_START, getCategoriesFromCookieSaga);
    yield takeLatest(actionTypes.ADD_CATEGORY_START, addCategorySaga);
    yield takeLatest(actionTypes.DELETE_CATEGORY_START, deleteCategorySaga);
    
}
export function* watchContacts() {
    yield takeLatest(actionTypes.GET_CONTACTS_START, getContactsSaga);
    yield takeLatest(actionTypes.GET_CONTACTS_FROM_SESSION_START, getContactsFromSessionStorageSaga);
    yield takeLatest(actionTypes.CREATE_CONTACT_START, createContactSaga);
    yield takeLatest(actionTypes.UPDATE_CONTACT_START, updateContactSaga);
    yield takeLatest(actionTypes.DELETE_CONTACT_START, deleteContactSaga);
}