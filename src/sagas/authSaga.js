import Cookies from "js-cookie";
import { put } from "redux-saga/effects";
import * as actionTypes from "../actions/actionConstants";
import axios from "axios";
var _ = require('lodash/core')


export function* signOutSaga(action) {
  yield Cookies.remove("token");
  yield Cookies.remove("queryId");
  yield sessionStorage.removeItem("user");

  yield put({
    type: actionTypes.SIGN_OUT_DONE,
  });
}

export function* signInSaga(action) {
  try {
    const response = yield axios.post(
      `${actionTypes.SERVER_URL}/api/v1/auth/login`,
      action.credentials
    );
    yield Cookies.set("token", response.data.token, { expires: 30 });
    yield Cookies.set("queryId", response.data.user.queryId, { expires: 30 });
    sessionStorage.setItem("user", JSON.stringify(response.data.user));
    yield put({
      type: actionTypes.SIGN_IN_DONE,
      payload: response.data,
    });
  } catch (error) {
    yield put({ type: actionTypes.SIGN_IN_ERROR, payload: error });
  }
}

export function* checkAuthStateSaga(action) {
  const token = yield Cookies.get("token");
  const queryId = yield Cookies.get("queryId");
  if (!(token && queryId)) {
    yield put({
      type: actionTypes.SIGN_OUT_START,
    });
  } else {
    yield put({
      type: actionTypes.SET_COOKIE_USER_START,
    });
  }
}

export function* setCookieUserSaga(action) {
  yield put({
    type: actionTypes.SET_COOKIE_USER_DONE,
    payload: { token: Cookies.get("token"), queryId: Cookies.get("queryId") },
  });
}

export function* signUpSaga(action) {
  try {
    const response = yield axios.post(
      `${actionTypes.SERVER_URL}/api/v1/auth/register`,
      action.credentials
    );

    yield Cookies.set("token", response.data.token, {
      expires: action.credentials.checked ? 365 : 30,
    });
    yield Cookies.set("queryId", response.data.user.queryId, {
      expires: action.credentials.checked ? 365 : 30,
    });
    yield sessionStorage.setItem("user", JSON.stringify(response.data.user));
    yield put({
      type: actionTypes.SIGN_UP_DONE,
      payload: response.data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.SIGN_UP_ERROR,
      payload: error,
    });
  }
}

export function* getUserDetailsSaga(action) {
  const user = sessionStorage.getItem("user");
  if (user) {
    const userJson = JSON.parse(user);
    yield put({
      type: actionTypes.GET_USER_DONE,
      payload: userJson,
    });
  } else {
    const token = Cookies.get("token");
    const queryId = Cookies.get("queryId");
    if (token && queryId) {
      try {
        const response = yield axios.get(
          `${actionTypes.SERVER_URL}/api/v1/auth/me`,
          {
            headers: {
              "Authorization": `Bearer ${token}`,
            },
            params: { queryId: queryId },
          }
        );
        
        sessionStorage.setItem("user", JSON.stringify(response.data.data));
        yield put({
          type: actionTypes.GET_USER_DONE,
          payload: response.data.data,
        });
      } catch (error) {
        yield put({
          type: actionTypes.GET_USER_ERROR,
          payload: error,
        });
      }
    }
  }
}

export function* getUserDetailsFromServerSaga(action) {
    const token = Cookies.get("token");
    const queryId = Cookies.get("queryId");
    if (token && queryId) {
      try {
        const response = yield axios.get(
          `${actionTypes.SERVER_URL}/api/v1/auth/me`,
          {
            headers: {
              "Authorization": `Bearer ${token}`,
              "Content-Type": 'application/json'
            },
            params: { queryId: queryId },
          }
        );
        sessionStorage.setItem("user", JSON.stringify(response.data.data));
        yield put({
          type: actionTypes.GET_USER_DONE,
          payload: response.data.data,
        });
      } catch (error) {
        yield put({
          type: actionTypes.GET_USER_ERROR,
          payload: error,
        });
      }
    }
}

export function* updateUserDetailsSaga(action) {
  
  let newObj = {};
  _.forEach(action.details, (value, key) => {
    if (value.length > 0) {
      newObj[key] = value;
    }
  });

    const token = Cookies.get("token");
    const queryId = Cookies.get("queryId");
    if (token && queryId) {
      try {
        const response = yield axios.put(
          `${actionTypes.SERVER_URL}/api/v1/auth/updateDetails`,
          newObj,
          {
            headers: {
              "Authorization": `Bearer ${token}`,
              "Content-Type": 'application/json'
            },
            params: { queryId: queryId },
          }
        );

        sessionStorage.setItem("user", JSON.stringify(response.data.data));
        yield put({
          type: actionTypes.GET_USER_DONE,
          payload: response.data.data,
          update: { updated: true }
        });

      } catch (error) {
        yield put({
          type: actionTypes.GET_USER_ERROR,
          payload: error,
        });
      }
    }

}
