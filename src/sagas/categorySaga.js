import { put } from "redux-saga/effects";
import axios from "axios";
import Cookies from "js-cookie";
import * as actionTypes from "../actions/actionConstants";

export function* getCategoriesSaga(action) {
  try {
    const response = yield axios.get(
      `${actionTypes.SERVER_URL}/api/v1/categories`
    );
    console.log(response);
      // response boş arrayse koymasın cookie ye
    if (response.data.data.length > 0) {
      yield sessionStorage.setItem("categories", JSON.stringify(response.data.data));
    } else {
      yield sessionStorage.removeItem('categories')
    }
    
    yield put({
      type: actionTypes.GET_CATEGORIES_DONE,
      payload: response.data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_CATEGORIES_ERROR,
      payload: error,
    });
  }
}

export function* getCategoriesFromCookieSaga(action) {
  try {
    
    const categoriesString = yield sessionStorage.getItem("categories");
    const categoriesJson = JSON.parse(categoriesString);
    if (categoriesJson) {
      yield put({
        type: actionTypes.GET_CATEGORIES_DONE,
        payload: {
          data: categoriesJson,
        },
      });
    } else {
      yield put({
        type:actionTypes.GET_CATEGORIES_START,
      })
    }
  } catch (error) {
    yield put({
      type: actionTypes.GET_CATEGORIES_START,
    });
  }
}

export function* addCategorySaga(action) {
  try {
    // console.log(action);
    
    const response = yield axios.post(
      `${actionTypes.SERVER_URL}/api/v1/categories`,
      { name: action.name },
      {
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${action.token}`,
        },
        params: { queryId: action.queryId }
      }
    );
      if (sessionStorage.getItem('categories')) {
        const categoriesString = yield sessionStorage.getItem("categories");
        const categoriesJson = JSON.parse(categoriesString);
        const newCategories = categoriesJson.concat([response.data.data]);
        sessionStorage.setItem("categories", JSON.stringify(newCategories));
      } else {
        sessionStorage.setItem("categories", JSON.stringify([response.data.data]));
      }

    yield put({
      type: actionTypes.ADD_CATEGORY_DONE,
      payload: response.data.data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.ADD_CATEGORY_ERROR,
      payload: error,
    });
  }
}

export function* deleteCategorySaga(action) {
  try {
    const response = yield axios.delete(
      `${actionTypes.SERVER_URL}/api/v1/categories/${action._id}`,
      {
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${action.token}`,
        },
        params: { queryId: action.queryId }
      }
    );
console.log(response);

    yield put({
      type: actionTypes.DELETE_CATEGORY_DONE,
      // payload: response.data.data,
    });
    yield put({
      type: actionTypes.GET_CATEGORIES_START,
    });
  } catch (error) {
    yield put({
      type: actionTypes.DELETE_CATEGORY_ERROR,
      payload: error,
    });
  }
}
