import { put, delay, call } from "redux-saga/effects";
import * as actionTypes from "../actions/actionConstants";
import axios from "axios";
import Cookies from "js-cookie";

export function* getItemsSaga(action) {
  
  try {
    const response = yield axios.get(
      `${actionTypes.SERVER_URL}/api/v1/ads?search=${action.searchQuery}`
    );

    yield put({
      type: actionTypes.GET_ITEMS_DONE,
      payload: response.data.data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_ITEMS_ERROR,
      payload: error,
    });
  }
}
export function* searchItemsSaga(searchQuery) {
  yield delay(500);
  // aşağıda put ile çağırınca searchquery i geçemedim diğerine
  yield call(getItemsSaga, searchQuery)
}

export function* getItemByIdSaga(action) {
  try {
    const response = yield axios.get(
      `${actionTypes.SERVER_URL}/api/v1/ads/${action.id}`
    );
    yield put({
      type: actionTypes.GET_ITEM_BY_ID_DONE,
      payload: response.data.data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_ITEM_BY_ID_ERROR,
      payload: error,
    });
  }
}

export function* getItemsByCategorySaga(action) {
  try {
    const response = yield axios.get(
      `${actionTypes.SERVER_URL}/api/v1/ads?category=${action.category}`
    );

    yield put({
      type: actionTypes.GET_ITEM_BY_CATEGORY_DONE,
      payload: response.data.data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_ITEM_BY_CATEGORY_ERROR,
      payload: error,
    });
  }
}

export function* addItemSaga(action) {
  try {
    const response = yield axios.post(
      `${actionTypes.SERVER_URL}/api/v1/ads`,
      action.item,
      {
        headers: {
          "Content-Type": "multipart/form-data",
          "Authorization": `Bearer ${Cookies.get("token")}`,
        },
      }
    );

    yield put({
      type: actionTypes.ADD_ITEM_DONE,
      payload: response.data.data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.ADD_ITEM_ERROR,
      payload: error,
    });
  }
}

export function* updateItemSaga(action) {
  
  try {
    const response = yield axios.put(
      `${actionTypes.SERVER_URL}/api/v1/ads/${action.adId}`,
      action.formData,
      {
        headers: {
          "Authorization": `Bearer ${Cookies.get("token")}`,
        },
        // BURADA DATA GONDERİRSEN formdatayı eziyor QUERY PARAM gidiyorsa sorun yok
      }
    );
    yield put({
      type: actionTypes.UPDATE_ITEM_BY_ID_DONE,
      payload: response.data.data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.UPDATE_ITEM_BY_ID_ERROR,
      payload: error,
    });
  }
}

export function* deletePhotoByIdSaga(action) {

  try {
    const queryId = yield Cookies.get("queryId");
    const response = yield axios.delete(`${actionTypes.SERVER_URL}/api/v1/ads/${action.adId}/photo/${action.photoName}`, {
      headers: {
        "Authorization": `Bearer ${Cookies.get("token")}`,
      },
      data: { queryId: queryId },
    })
    yield put({
      type: actionTypes.DELETE_PHOTO_WITH_ID_DONE,
      payload: response.data
    })
  } catch (error) {
    yield put({
      type: actionTypes.DELETE_PHOTO_WITH_ID_ERROR,
      payload: error
    })
  }
}

export function* deleteItemById(action) {

  try {
    const queryId = yield Cookies.get("queryId");

    const response = yield axios.delete(`${actionTypes.SERVER_URL}/api/v1/ads/${action.id}`, {
      headers: {
        "Authorization": `Bearer ${Cookies.get("token")}`,
      },
      data: { queryId: queryId },
    })
    yield put({
      type: actionTypes.DELETE_ITEM_DONE,
      payload: response.data
    })
  } catch (error) {
    yield put({
      type: actionTypes.DELETE_ITEM_ERROR,
      payload: error
    })
  }

}

// iptal
// export function* uploadNewPhotoToAdSaga(action) {
//     try {
//         const response = yield axios.put(`${actionTypes.SERVER_URL}/api/v1/ads/${action.adId}/photo`, action.photos, {
//           headers: {
//             Authorization: `Bearer ${Cookies.get("token")}`,
//           },
//         })

//         yield put({
//             type: actionTypes.UPDATE_PHOTOS_OF_AN_ADD_DONE,
//             payload: response
//         })
//     } catch (error) {
//         yield put({
//             type: actionTypes.UPDATE_PHOTOS_OF_AN_ADD_ERROR,
//             payload: error
//         })
//     }
// }
