import React from "react";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { createBrowserHistory } from "history";

import "./App.css";
import "./shop-homepage.css";
import Appbar from "./components/appbar/Appbar";
import Footer from "./components/footer/Footer";
import Home from "./components/home/Home";
import AdDetailPage from "./components/addetail/AdDetailPage";
import EditAd from "./components/editAd/EditAd";
import NewAd from "./components/newAd/NewAd";
import Login from "./components/login/Login";
import Profile from "./components/profile/Profile";
import SignUp from "./components/signUp/SignUp";
import Contact from "./components/contact/Contact";

const history = createBrowserHistory();

const App = () => {
  //  "APP RENDER");
  return (
    <Router history={history}>
      <div className="">
        <Appbar />
      </div>
      <Switch >
        <Route 
          path="/"
          exact
          component={() => <Redirect to="/home" />}
        />
        <Route 
          path="/home"
          exact
          component={() => <Home />}
        />
        <Route 
          path="/home/:id/:slug"
          exact
          component={() => <AdDetailPage />}
        />
        <Route 
          path="/edit/:id"
          exact
          component={() => <EditAd />}
        />
        <Route 
          path="/new"
          exact
          component={() => <NewAd />}
        />
        <Route 
          path="/toma"
          exact
          component={() => <Login />}
        />
        <Route 
          path="/seroo"
          exact
          component={() => <SignUp />}
        />
        <Route 
          path="/me"
          exact
          component={() => <Profile />}
        />
        <Route 
          path="/contact"
          exact
          component={() => <Contact />}
        />
      </Switch>
        <Footer />
    </Router>
  );
};

export default App;
