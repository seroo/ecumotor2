import {
  GET_CONTACTS_START,
  GET_CONTACT_BY_ID_START,
  CREATE_CONTACT_START,
  UPDATE_CONTACT_START,
  GET_CONTACTS_FROM_SESSION_START,
  DELETE_CONTACT_START,
} from "./actionConstants";

export const getContacts = () => {
  return {
    type: GET_CONTACTS_START,
  };
};

export const getContactsFromSessionStorage = () => {
  return {
    type: GET_CONTACTS_FROM_SESSION_START,
  };
};

export const getContactById = (id) => {
  return {
    type: GET_CONTACT_BY_ID_START,
    id,
  };
};

export const createContact = (contact) => {
  return {
    type: CREATE_CONTACT_START,
    contact,
  };
};

export const updateContact = (updatedContact, _id) => {
  return {
    type: UPDATE_CONTACT_START,
    updatedContact,
    _id
  };
};

export const deleteContact = (_id) => {
  
  return {
    type: DELETE_CONTACT_START,
    _id,
  };
};
