import {
  GET_ITEMS_START,
  GET_ITEM_BY_ID_START,
  ADD_ITEM_START,
  DELETE_ITEM_START,
  DELETE_PHOTO_WITH_ID_START,
  UPDATE_ITEM_BY_ID_START,
  // GET_ITEM_BY_CATEGORY_START,
  // GET_ITEM_BY_CATEGORY_DONE,
  // GET_ITEM_BY_CATEGORY_ERROR,
  GET_ITEM_BY_CATEGORY_START,
  SEARCH_ITEMS_START,
} from "./actionConstants";

export const getItems = (searchQuery) => {
  //  searchQuery);

  return {
    type: GET_ITEMS_START,
    searchQuery,
  };
};

export const searchItems = (searchQuery) => {
  //  searchQuery);

  return {
    type: SEARCH_ITEMS_START,
    searchQuery,
  };
};

export const getItemById = (id) => {
  //  id);

  return {
    type: GET_ITEM_BY_ID_START,
    id,
  };
};

export const getItemsByCategory = (category) => {
  return {
    type: GET_ITEM_BY_CATEGORY_START,
    category,
  };
};

export const addItem = (item) => {
  return {
    type: ADD_ITEM_START,
    item
  };
};

export const deleteItemById = (id) => {
  return {
      type: DELETE_ITEM_START,
      id
    };
};
// delete one photo from an ad
export const deletePhotoById = (adId, photoName) => {
  return {
      type: DELETE_PHOTO_WITH_ID_START,
      adId,
      photoName
    };
};
export const updateItemById = (adId, updatedForm) => {
  
  const { header, price, description, category, files, queryId, contact } = updatedForm;
  let formData = new FormData();
  for (const key of Object.keys(files)) {
    formData.append("photos", files[key]);
  }
  formData.append("header", header);
  formData.append("price", price);
  formData.append("description", description);
  formData.append("category", category);
  formData.append("queryId", queryId);
  formData.append("contact", contact);

  return {
      type: UPDATE_ITEM_BY_ID_START,
      adId,
      formData
    };
    //  adId);
    //  ...updatedForm);

};
// iptal
// export const uploadNewPhotosToAd = (adId, photos) => {
//   return {
//       type: UPDATE_PHOTOS_OF_AN_ADD_START,
//       adId,
//       photos
//     };
// };
