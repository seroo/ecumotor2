import {
    GET_CATEGORIES_FROM_COOKIE_START,
    ADD_CATEGORY_START,
    DELETE_CATEGORY_START,
  } from "./actionConstants";

  import Cookies from 'js-cookie'
  
  // Eger kategori ekleme yaparsam queryId eklemeliyim
  // export const getCategories = () => {
  //   return {
  //       type: GET_CATEGORIES_START
  //     };
  // };
  export const getCategoriesFromCookie = () => {
    return {
        type: GET_CATEGORIES_FROM_COOKIE_START
      };
  };
  export const addCategory = (name) => {
    const tokenFromCookie = Cookies.get('token')
    const queryIdFromCookie = Cookies.get('queryId')
    return {
      type: ADD_CATEGORY_START,
      name,
      token: tokenFromCookie,
      queryId: queryIdFromCookie,
    }
  }
  
  export const deleteCategory = (_id) => {
    const tokenFromCookie = Cookies.get('token')
    const queryIdFromCookie = Cookies.get('queryId')
    return {
      type: DELETE_CATEGORY_START,
      _id,
      token: tokenFromCookie,
      queryId: queryIdFromCookie,
    }
  }