import {
  SIGN_IN_START,
  SIGN_OUT_START,
  CHECK_USER_SIGNED_IN_START,
  GET_USER_START,
  UPDATE_USER_START,
  SIGN_UP_START,
  SET_COOKIE_USER_START,
  GET_USER_FROM_SERVER_START,
} from "./actionConstants";

export const signIn = (credentials) => {
  return {
    type: SIGN_IN_START,
    credentials,
  };
};
export const signUp = (credentials) => {
  return {
    type: SIGN_UP_START,
    credentials,
  };
};
export const isSignedIn = () => {
  return {
    type: CHECK_USER_SIGNED_IN_START,
  };
};

export const getUserDetails = () => {
  return {
    type: GET_USER_START,
  };
};

export const getUserDetailsFromServer = () => {
  return {
    type: GET_USER_FROM_SERVER_START,
  };
};
export const updateUserDetails = (details) => {
  return {
    type: UPDATE_USER_START,
    details
  };
};

export const signOut = () => {
  return {
    type: SIGN_OUT_START,
  };
};
export const setCookieUser = () => {
  return {
    type: SET_COOKIE_USER_START,
  };
};
