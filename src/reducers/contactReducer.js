import {
  GET_CONTACTS_START,
  GET_CONTACTS_DONE,
  GET_CONTACTS_ERROR,
  GET_CONTACT_BY_ID_START,
  GET_CONTACT_BY_ID_DONE,
  GET_CONTACT_BY_ID_ERROR,
  CREATE_CONTACT_START,
  CREATE_CONTACT_DONE,
  CREATE_CONTACT_ERROR,
  UPDATE_CONTACT_START,
  UPDATE_CONTACT_DONE,
  UPDATE_CONTACT_ERROR,
  DELETE_CONTACT_START,
  DELETE_CONTACT_DONE,
  DELETE_CONTACT_ERROR,
} from "../actions/actionConstants";

const initialState = {
  loading: false,
  loaded: false,
  error: null,
  saving: false,
  saved: false,
  deleting: false,
  deleted: false,
  updating: false,
  updated: false,
  contacts: [],
  selectedContact: null,
};

const contactReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CONTACTS_START:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null,
      };
    case GET_CONTACTS_DONE:
      return {
        ...state,
        loaded: true,
        loading: false,
        error: null,
        contacts: action.payload,
      };
    case GET_CONTACTS_ERROR:
      return {
        ...state,
        loaded: false,
        loading: false,
        error: action.payload,
      };
    case GET_CONTACT_BY_ID_START:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null,
      };
    case GET_CONTACT_BY_ID_DONE:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: null,
        selectedContact: action.payload,
      };
    case GET_CONTACT_BY_ID_ERROR:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.payload,
      };
    case CREATE_CONTACT_START:
      return {
        ...state,
        saving: true,
        saved: false,
        error: null,
      };
    case CREATE_CONTACT_DONE:
      return {
        ...state,
        saving: false,
        saved: true,
        error: null,
        contacts: state.contacts.concat([action.payload]),
      };
    case CREATE_CONTACT_ERROR:
      return {
        ...state,
        saving: false,
        saved: false,
        error: action.payload,
      };
    case UPDATE_CONTACT_START:
      return {
        ...state,
        updated: false,
        updating: true,
        error: null,
      };
    case UPDATE_CONTACT_DONE:
      return {
        ...state,
        updated: true,
        updating: false,
        error: null,
      };
    case UPDATE_CONTACT_ERROR:
      return {
        ...state,
        updated: false,
        updating: false,
        error: action.payload,
      };
    case DELETE_CONTACT_START:
      return {
        ...state,
        deleting: true,
        deleted: false,
        error: null,
      };
    case DELETE_CONTACT_DONE:
      return {
        ...state,
        deleting: false,
        deleted: true,
        error: null,
      };
    case DELETE_CONTACT_ERROR:
      return {
        ...state,
        deleting: false,
        deleted: false,
        error: action.payload,
      };

    default:
      return state;
  }
};

export default contactReducer;
