import {
  GET_ITEMS_START,
  GET_ITEMS_DONE,
  GET_ITEMS_ERROR,
  GET_ITEM_BY_CATEGORY_START,
  GET_ITEM_BY_CATEGORY_DONE,
  GET_ITEM_BY_CATEGORY_ERROR,
  ADD_ITEM_START,
  ADD_ITEM_DONE,
  ADD_ITEM_ERROR,
  DELETE_ITEM_START,
  DELETE_ITEM_DONE,
  DELETE_ITEM_ERROR,
  DELETE_PHOTO_WITH_ID_START,
  DELETE_PHOTO_WITH_ID_DONE,
  DELETE_PHOTO_WITH_ID_ERROR,
  UPDATE_ITEM_BY_ID_START,
  UPDATE_ITEM_BY_ID_DONE,
  UPDATE_ITEM_BY_ID_ERROR,
  SET_SELECTED_ITEM_INDEX,
  CLEAR_SELECTED_ITEM_INDEX,
  SET_SELECTED_CATEGORY,
  CLEAR_SELECTED_CATEGORY,
  UPDATE_PHOTOS_OF_AN_ADD_START,
  UPDATE_PHOTOS_OF_AN_ADD_DONE,
  UPDATE_PHOTOS_OF_AN_ADD_ERROR,
  GET_ITEM_BY_ID_START,
  GET_ITEM_BY_ID_DONE,
  GET_ITEM_BY_ID_ERROR,
} from "../actions/actionConstants";

const initialState = {
  loading: false,
  selectedItemIndex: -1,
  selectedCategory: null,
  loaded: false,
  updating: false,
  updated: false,
  updatedItem: null,
  error: null,
  saving: false,
  saved: false,
  deleting: false,
  deleted: false,
  photoDeleting: false,
  photoDeleted: false,
  photoUpdating: false,
  photoUpdated: false,
  itemById: null,
  itemByIdLoading: false,
  itemByIdLoaded: false,
  itemByIdError: null,
  items: [],
};

const itemReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ITEMS_START:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null,
        updating: false,
        updated: false,
        saving: false,
        saved: false,
        deleting: false,
        deleted: false,
      };
    case GET_ITEMS_DONE:
      return {
        ...state,
        loading: false,
        loaded: true,
        items: action.payload,
        error: null,
      };
    case GET_ITEMS_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload.response.data.error || "BILINMEYEN HATA",
        loaded: false,
      };
    case GET_ITEM_BY_ID_START:
      return {
        ...state,
        itemById: null,
        itemByIdLoading: true,
        itemByIdLoaded: false,
        itemByIdError: null,
      };
    case GET_ITEM_BY_ID_DONE:
      return {
        ...state,
        itemById: action.payload,
        itemByIdLoading: false,
        itemByIdLoaded: true,
        itemByIdError: null,
      };
    case GET_ITEM_BY_ID_ERROR:
      return {
        ...state,
        itemById: null,
        itemByIdLoading: false,
        itemByIdLoaded: false,
        itemByIdError: action.payload,
      };
    case GET_ITEM_BY_CATEGORY_START:
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null,
        updating: false,
        updated: false,
        saving: false,
        saved: false,
        deleting: false,
        deleted: false,
      };
    case GET_ITEM_BY_CATEGORY_DONE:
      return {
        ...state,
        loading: false,
        loaded: true,
        items: action.payload,
        error: null,
      };
    case GET_ITEM_BY_CATEGORY_ERROR:
      return { ...state, loading: false, loaded: false, error: action.payload };
    case ADD_ITEM_START:
      return {
        ...state,
        saving: true,
        saved: false,
        error: null,
        updating: false,
        updated: false,
        deleting: false,
        deleted: false,
      };
    case ADD_ITEM_DONE:
      return {
        ...state,
        saving: false,
        saved: true,
        error: null,
      };
    case ADD_ITEM_ERROR:
      return {
        ...state,
        error: action.payload,
        saving: false,
        saved: false,
      };
    case DELETE_ITEM_START:
      return { ...state, deleting: true, deleted: false };
    case DELETE_ITEM_DONE:
      return { ...state, deleting: false, deleted: true, error: null };
    case DELETE_ITEM_ERROR:
      return {
        ...state,
        loading: false,
        loaded: false,
        deleted: false,
        error: action.payload,
      };
    case UPDATE_ITEM_BY_ID_START:
      return { ...state, updating: true, updated: false, updatedItem: null };
    case UPDATE_ITEM_BY_ID_DONE:
      return {
        ...state,
        updating: false,
        updated: true,
        error: null,
        updatedItem: action.payload,
      };
    case UPDATE_ITEM_BY_ID_ERROR:
      return {
        ...state,
        updating: false,
        updated: false,
        error: action.payload,
      };
    case DELETE_PHOTO_WITH_ID_START:
      return { ...state, photoDeleting: true, photoDeleted: false };
    case DELETE_PHOTO_WITH_ID_DONE:
      return {
        ...state,
        photoDeleting: false,
        photoDeleted: true,
        error: null,
      };
    case DELETE_PHOTO_WITH_ID_ERROR:
      return {
        ...state,
        photoDeleting: false,
        photoDeleted: false,
        error: action.payload,
      };
    case UPDATE_PHOTOS_OF_AN_ADD_START:
      return { ...state, photoUpdating: true, photoUpdated: false };
    case UPDATE_PHOTOS_OF_AN_ADD_DONE:
      return {
        ...state,
        photoUpdating: false,
        photoUpdated: true,
        error: null,
      };
    case UPDATE_PHOTOS_OF_AN_ADD_ERROR:
      return {
        ...state,
        photoUpdating: false,
        photoUpdated: false,
        error: action.payload,
      };
    case SET_SELECTED_ITEM_INDEX:
      return {
        ...state,
        selectedItemIndex: action.payload,
      };
    case CLEAR_SELECTED_ITEM_INDEX:
      return {
        ...state,
        selectedItemIndex: action.payload,
      };
    case SET_SELECTED_CATEGORY:
      return {
        ...state,
        selectedCategory: action.payload,
      };
    case CLEAR_SELECTED_CATEGORY:
      return {
        ...state,
        selectedCategory: null,
      };
    default:
      return state;
  }
};

export default itemReducer;
