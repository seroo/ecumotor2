import { combineReducers } from 'redux';
import itemReducer from './itemReducer';
import categoryReducer from './categoryReducer';
import authReducer from './authReducer';
import contactReducer from './contactReducer';

const rootReducer = combineReducers({
  getItems: itemReducer,
  categories: categoryReducer,
  auth: authReducer,
  contacts: contactReducer,
});

export default rootReducer;
