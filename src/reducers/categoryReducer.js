import {
  GET_CATEGORIES_START,
  GET_CATEGORIES_DONE,
  GET_CATEGORIES_ERROR,
  ADD_CATEGORY_START,
  ADD_CATEGORY_DONE,
  ADD_CATEGORY_ERROR,
  DELETE_CATEGORY_START,
  DELETE_CATEGORY_DONE,
  DELETE_CATEGORY_ERROR,
} from "../actions/actionConstants";

const initialState = {
  loading: false,
  loaded: false,
  error: null,
  saving: false,
  saved: false,
  deleting: false,
  deleted: false,
  categories: [],
};

const categoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CATEGORIES_START:
      return { ...state, loading: true, loaded: false, error: null };

    case GET_CATEGORIES_DONE:
      return {
        ...state,
        loading: false,
        loaded: true,
        categories: action.payload.data,
      };

    case GET_CATEGORIES_ERROR:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.payload.response.data.error || "BILINMEYEN HATA",
      };
    case ADD_CATEGORY_START:
      return {
        ...state,
        saving: true,
        saved: false,
        error:null,
      }
      case ADD_CATEGORY_DONE:
      return {
        ...state,
        saving: false,
        saved: true,
        categories: state.categories.concat([action.payload])
      }
      case ADD_CATEGORY_ERROR:
      return {
        ...state,
        saving: false,
        saved: false,
        error: action.payload.response.data.error || "BILINMEYEN HATA",
      }
      case DELETE_CATEGORY_START:
      return {
        ...state,
        deleting: true,
        deleted: false,
        error:null,
      }
      case DELETE_CATEGORY_DONE:
      return {
        ...state,
        deleting: false,
        deleted: true,
        error: null,
      }
      case DELETE_CATEGORY_ERROR:
      return {
        ...state,
        deleting: false,
        deleted: false,
        error: action.payload.response.data.error || "BILINMEYEN HATA",
      }
    default:
      return state;
  }
};
export default categoryReducer;
