import {
    SIGN_IN_START,
    SIGN_IN_DONE,
    SIGN_IN_ERROR,
    SIGN_UP_START,
    SIGN_UP_DONE,
    SIGN_UP_ERROR,
    SET_COOKIE_USER_DONE,
    CHECK_USER_SIGNED_IN_START,
    CHECK_USER_SIGNED_IN_DONE,
    CHECK_USER_SIGNED_IN_ERROR,
    GET_USER_START,
    GET_USER_DONE,
    GET_USER_ERROR,
    SIGN_OUT_START,
    SIGN_OUT_DONE,
    UPDATE_USER_START,
    // 
  } from "../actions/actionConstants";
  
  
  
  const initialState = {
    isSignedIn: false,
    error: null,
    token: null,
    updated: false,
    loaded: false,
    didSigningOut: false,
    loading: false,
    updating: false,
    user: null,
    queryId: null,
    userUpdating: false,
    userUpdated: false,

  };
  
  const authReducer = (state = initialState, action) => {
    switch (action.type) {
      case SIGN_IN_START:
        return {
          ...state,
          isSignedIn: false,
          queryId: null,
          user: null,
          error: null,
          loading: true,
          loaded: false,
        };
      case SIGN_IN_DONE:
        return {
          ...state,
          isSignedIn: true,
          token: action.payload.token,
          error: null,
          loading: false,
          loaded: true,
          user: action.payload.user,
          queryId: action.payload.user.queryId
        };
      case SIGN_IN_ERROR:
        
        return {
          ...state,
          isSignedIn: false,
          token: null,
          error: action.payload.response.data.error || "BILINMEYEN HATA",
          loading: false,
          user: null,
        };
      case SIGN_UP_START:
        return {
          ...state,
          isSignedIn: false,
          user: null,
          error: null,
          loading: true,
          loaded: false,
        };
      case SIGN_UP_DONE:
        return {
          ...state,
          isSignedIn: true,
          token: action.payload.token,
          error: null,
          loading: false,
          loaded: true,
          user: action.payload.user
        };
      case SIGN_UP_ERROR:
        return {
          ...state,
          isSignedIn: false,
          token: null,
          error: action.payload.response.data.error || "BILINMEYEN HATA",
          loading: false,
          user: null
        };
      case SET_COOKIE_USER_DONE:
        return {
          ...state,
          isSignedIn: true,
          token: action.payload.token,
          queryId: action.payload.queryId
        };
      case CHECK_USER_SIGNED_IN_START:
        return {
          ...state,
          isSignedIn: false,
        };
      case CHECK_USER_SIGNED_IN_DONE:
        return {
          ...state,
          isSignedIn: true,
          user: action.payload
        };
      case CHECK_USER_SIGNED_IN_ERROR:
        return {
          ...state,
          isSignedIn: false,
          token: null,
          error: action.payload.response.data.error || "BILINMEYEN HATA",
          loading: false,
          loaded: false,
        };
      case UPDATE_USER_START:
        return {
          ...state,
          userUpdating: true,
        };
        case GET_USER_START:
        return {
          ...state,
          loading: true,
          userUpdated: false,
        };
      case GET_USER_DONE:
        return {
          ...state,
          loaded: true,
          loading: false,
          userUpdated: action.update && action.update.updated,
          userUpdating: false,
          user: action.payload,
          isSignedIn: true
        };
      case GET_USER_ERROR:
        return {
          ...state,
          error: action.payload.response.data.error || "BILINMEYEN HATA",
        };
      case SIGN_OUT_START:
        return {
          ...state,
        };
      case SIGN_OUT_DONE:
        return {
          ...state,
          isSignedIn: false,
          error: null,
          token: null,
          updated: false,
          loaded: false,
          didSigningOut: false,
          loading: false,
          updating: false,
          user: null,
        };
      default:
        return state;
    }
  };
  export default authReducer;
  