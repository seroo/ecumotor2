export const getCategory = (categories, id) => {
    // console.log(categories);
    // console.log(id);
    
    for (let index = 0; index < categories.length; index++) {
        if (categories[index]._id === id) {
            return categories[index]
        }
    }
    return null
}
export const getCategoryName = (categories, id) => {
    // console.log(categories);
    // console.log(id);
    
    for (let index = 0; index < categories.length; index++) {
        if (categories[index]._id === id) {
            return categories[index].name
        }
    }
    return null
}

export const getContactName = (contacts, id) => {
    console.log(contacts);
    console.log(id);
    
    
    for (let index = 0; index < contacts.length; index++) {
        if (contacts[index]._id === id) {
            
            return contacts[index].name
        }
    }
    return null
}
export const comfirmSelectedCategory = (categories, id) => {
    // console.log("HELPERS");
    // console.log(categories);
    // console.log(id);
    
    
    for (let index = 0; index < categories.length; index++) {
        
        if (categories[index]._id === id) {
            
            return true
        }

    }
    // console.log("FALSE");
    
    return false
}